package com.dingsgo.oufyp;

import java.util.ArrayList;

/**
 * Created by dings on 31/1/2017.
 */
public interface DBManager<T> {
    public abstract long insert(T t);
    public abstract ArrayList<T> getAll();
    public abstract T get(String name);
    public abstract ArrayList<T> query(String keyword);
}
