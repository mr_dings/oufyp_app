package com.dingsgo.oufyp;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by dings on 6/2/2017.
 */

public class Schedule implements Serializable{

    public static final int TOTAL_MINUTE_IN_DAY = 780;
    public static final int MORNING_MINUTE = 180;
    public static final int AFTERNOON_MINUTE = 360;
    public static final int NIGHT_MINUTE = 240;

    public static final int TIME_MORNING = 0;
    public static final int TIME_AFTERNOON = 1;
    public static final int TIME_NIGHT = 2;

    private static final String TAG = "Schedule";

    private static final long serialVersionUID = 5932185325L;
    private String name;
    private long startDate;
    private long endDate;
    private int day_period;
    private ArrayList<DaySchedule> dayList;

    public Schedule(String name, long startDate, int day_period) {
        this.name = name;
        setDatePeriod(startDate, day_period);
        dayList = new ArrayList<>();
        for(int i = 0; i < day_period; i ++){
            dayList.add(i, new DaySchedule(this.startDate + (i * 24 * 60 * 60 * 1000)));
        }
    }

    public void setDatePeriod(long startDate, int day_period){
        this.startDate = startDate;
        this.day_period = day_period;
        this.endDate = this.startDate + this.day_period * 24 * 60 * 60 * 1000;
    }

    public void add(int day, int timePeriod, Place place){
        if(day < 1 || day > this.day_period){
            throw new RuntimeException("day of period invalid. current is " + this.day_period + " but input day is " + day);
        }
        if(place == null ){
            throw new RuntimeException("place can not be null");
        }
        DaySchedule curr_daySchedule = getSchedule(day);
        curr_daySchedule.add(timePeriod, place);
        Log.i(TAG, "add: day " + day + " " + timePeriod + " has been add -> " + place.toString());
    }

    public void remove(int day, int timePeriod, int index){
        if(day < 1 || day > this.day_period){
            throw new RuntimeException("day of period invalid. current is " + this.day_period + " but input day is " + day);
        }
        DaySchedule curr_day = dayList.get(--day);
        if(curr_day == null){
            return;
        }
        curr_day.remove(timePeriod, index);
    }

    public DaySchedule getSchedule(int day){
        return dayList.get(--day);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getStartDate() {
        return startDate;
    }

    public long getEndDate() {
        return endDate;
    }

    public int getDay_period() {
        return day_period;
    }

    public static String toJSON(Schedule schedule){
        Gson gson = new Gson();
        String json = gson.toJson(schedule);
        Log.i(TAG, "toJSON: " + json);

        return json;
    }

    public static Schedule fromJSON(String scheduleJSON){
        Gson gson = new Gson();
        Schedule schedule = gson.fromJson(scheduleJSON, Schedule.class);
        return schedule;
    }

    public static void saveAsLocalStorage(Context context, Schedule schedule) throws IOException {
        String scheduleJsonString = toJSON(schedule);
        File path = context.getDir("schedules", context.MODE_PRIVATE);
        File file = new File(path, schedule.getName() + ".json");
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(scheduleJsonString.getBytes());
        fos.close();
    }

    public static boolean deleteFromLocalStorage(Context context, String name){
        File path = context.getDir("schedules", context.MODE_PRIVATE);
        File file = new File(path, name + ".json");
        return file.delete();
    }

    public static ArrayList<Schedule> loadFromLocalStorage(Context context) throws IOException {
        File dir = context.getDir("schedules", context.MODE_PRIVATE);
        File[] files = dir.listFiles();
        ArrayList<Schedule> list = new ArrayList<>();
        FileInputStream fis = null;
        BufferedInputStream bis = null;
        int len = 0;
        byte[] buffer = new byte[1024];
        String content;
        for(File inFile : files){
            if(inFile.isFile()){
                fis = new FileInputStream(inFile);
                bis = new BufferedInputStream(fis);
                content = "";
                while((len = bis.read(buffer)) != -1){
                    content+= new String(buffer, 0, len);
                }
                list.add(Schedule.fromJSON(content));
            }
        }
        return list;
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "name='" + name + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", day_period=" + day_period +
                ", dayList=" + dayList +
                '}';
    }

    public class DaySchedule implements Serializable{

        private static final long serialVersionUID = 935839851L;

        long date;
        ArrayList<Place> morningList;
        ArrayList<Place> afternoonList;
        ArrayList<Place> nightList;

        public DaySchedule(long date){
            this.date = date;
            morningList = new ArrayList<>();
            afternoonList = new ArrayList<>();
            nightList = new ArrayList<>();
        }

        private void add(int timePeriod, Place place, int index){
            switch (timePeriod){
                case TIME_MORNING:
                    morningList.add(index, place);
                    break;
                case TIME_AFTERNOON:
                    afternoonList.add(index,place);
                    break;
                case TIME_NIGHT:
                    nightList.add(index,place);
                    break;
                default:
                    throw new RuntimeException("input time don't exist");
            }
        }

        private void add(int timePeriod, Place place){
            switch (timePeriod){
                case TIME_MORNING:
                    morningList.add(place);
                    break;
                case TIME_AFTERNOON:
                    afternoonList.add(place);
                    break;
                case TIME_NIGHT:
                    nightList.add(place);
                    break;
                default:
                    throw new RuntimeException("input time don't exist");
            }
        }

        private void remove(int timePeriod, int index) {
            Place place;
            switch (timePeriod){
                case TIME_MORNING:
                    place = morningList.remove(index);
                    break;
                case TIME_AFTERNOON:
                    place = afternoonList.remove(index);
                    break;
                case TIME_NIGHT:
                    place = nightList.remove(index);
                    break;
                default:
                    throw new RuntimeException("input time don't exist");
            }
        }

        public ArrayList<Place> getPlaceList(){
            ArrayList<Place> places = new ArrayList<>();
            places.addAll(morningList);
            places.addAll(afternoonList);
            places.addAll(nightList);
            return places;
        }

        public void setList(int timePeriod, ArrayList<Place> places){
            switch (timePeriod){
                case TIME_MORNING:
                    this.morningList = places;
                case TIME_AFTERNOON:
                    this.afternoonList = places;
                case TIME_NIGHT:
                    this.nightList = places;
                default:
                    throw new RuntimeException("input time don't exist");
            }
        }

        public ArrayList<Place> getTimeSchedule(int timePeriod){
            switch (timePeriod){
                case TIME_MORNING:
                    return morningList;
                case TIME_AFTERNOON:
                    return afternoonList;
                case TIME_NIGHT:
                    return nightList;
                default:
                    throw new RuntimeException("input time don't exist");
            }
        }

        public int getScheduleTimeRequire(int timePeriod){
            ArrayList<Place> list;
            switch (timePeriod){
                case TIME_MORNING:
                    list = morningList;
                    break;
                case TIME_AFTERNOON:
                    list = afternoonList;
                    break;
                case TIME_NIGHT:
                    list = nightList;
                    break;
                default:
                    throw new RuntimeException("input time don't exist");
            }
            int totalMinute = 0;
            for(Place place : list){
                totalMinute += place.recommendMinute;
            }
            return totalMinute;
        }

        public int getScheduleTotalTime(){
            int total = 0;
            for(int i = 0 ; i <= 2; i ++)
                total += getScheduleTimeRequire(i);
            return total;
        }
        public long getDate() {
            return date;
        }

        public int getTotalPlaceCount(){
            return morningList.size() + afternoonList.size() + nightList.size();
        }
    }
}
