package com.dingsgo.oufyp;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by dings on 24/1/2017.
 */

public class FypDB extends SQLiteOpenHelper {

    public static final int DB_VERSION = 1;
    public final static String DATABASE_NAME = "fyp.db";
    public static final String TOURISTS_TABLE = "tourists";
    public static final String EVENT_TABLE = "events";

    public FypDB(Context context){
        super(context, DATABASE_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String tourists_sql = "CREATE TABLE `tourists` (\n" +
                "  `name` varchar(100) NOT NULL,\n" +
                "  `district` varchar(100) NOT NULL,\n" +
                "  `address` varchar(100) NOT NULL,\n" +
                "  `description` varchar(100) NOT NULL,\n" +
                "  `detail` text NOT NULL,\n" +
                "  `website` varchar(300) DEFAULT NULL,\n" +
                "  `lat` double NOT NULL,\n" +
                "  `lon` double NOT NULL,\n" +
                "  `tel` varchar(20) DEFAULT NULL,\n" +
                "  `imgSrc` varchar(200) DEFAULT NULL,\n" +
                "  `tag1` varchar(255) DEFAULT NULL,\n" +
                "  `tag2` varchar(255) DEFAULT NULL,\n" +
                "  `tag3` varchar(255) DEFAULT NULL,\n" +
                "  `openHour` int(11) NOT NULL DEFAULT '0',\n" +
                "  `endHour` int(11) NOT NULL DEFAULT '2400',\n" +
                "  `recommendMinute` int(11) NOT NULL DEFAULT '0'," +
                "  PRIMARY KEY (`name`)\n" +
                ")";

        String events_sql = "CREATE TABLE `events` (\n" +
                "  `name` varchar(200) NOT NULL,\n" +
                "  `address` text NOT NULL,\n" +
                "  `description` varchar(100) DEFAULT NULL,\n" +
                "  `detail` text NOT NULL,\n" +
                "  `website` varchar(300) DEFAULT NULL,\n" +
                "  `lat` double DEFAULT NULL,\n" +
                "  `lon` double DEFAULT NULL,\n" +
                "  `tel` text,\n" +
                "  `imgSrc` varchar(200) NOT NULL,\n" +
                "  `startDate` bigint(20) DEFAULT NULL,\n" +
                "  `endDate` bigint(20) DEFAULT NULL,\n" +
                "  `openingHoursDescription` text,\n" +
                "  `admission` varchar(200) DEFAULT NULL,\n" +
                "  `openHour` int(11) NOT NULL DEFAULT '0',\n" +
                "  `endHour` int(11) NOT NULL DEFAULT '2400',\n" +
                "  `recommendMinute` int(11) NOT NULL DEFAULT '0'," +
                "  PRIMARY KEY (`name`)\n" +
                ") ";

        db.execSQL(tourists_sql);
        db.execSQL(events_sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }



}
