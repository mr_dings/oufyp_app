package com.dingsgo.oufyp;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dingsgo.oufyp.scheduleProcess.NewScheduleActivity;
import com.dingsgo.oufyp.scheduleProcess.ScheduleEditActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private static final String TAG = "HomeFragment";

    private Button btn_schedule_preview, btn_attraction_preview, btn_event_preview;
    private TextView tv_schedule_description, tv_attraction_description, tv_event_description;
    private LinearLayout attraction_zone, event_zone, schedule_zone;

    private Place ram_place;
    private Event ram_event;
    private Schedule ram_schedule;

    private EventManager em;
    private TouristManager tm;
    private ArrayList<Schedule> mList;

    private ImageManager im;

    public HomeFragment(){
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle("Hello HK Travel Guide");
    }

    public void ramdom(){

        ram_event = em.getRamdomEvent();
        ram_place = tm.getRamdomTourist();
        Random random = new Random();
        try{
            int rand = random.nextInt(mList.size());
            ram_schedule = mList.get(rand);
        }catch (Exception e){
            ram_schedule = null;
        }
//        Log.i(TAG, "ramdom: " + ram_place.toString());
//        Log.i(TAG, "ramdom: " + ram_event.toString());
//        Log.i(TAG, "ramdom: " + ram_schedule.toString());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        em = new EventManager(getContext());
        tm = new TouristManager(getContext());
        im = new ImageManager(getContext());
        try {
            mList = Schedule.loadFromLocalStorage(getContext());
        } catch (IOException e) {
            e.printStackTrace();
        }

        View v = inflater.inflate(R.layout.fragment_home, container, false);

        schedule_zone = (LinearLayout) v.findViewById(R.id.schedule_zone);
        attraction_zone = (LinearLayout) v.findViewById(R.id.attraction_zone);
        event_zone = (LinearLayout) v.findViewById(R.id.event_zone);

        btn_schedule_preview = (Button) v.findViewById(R.id.btn_schedule_preview);
        btn_attraction_preview = (Button) v.findViewById(R.id.btn_attraction_preview);
        btn_event_preview = (Button) v.findViewById(R.id.btn_event_preview);

        tv_schedule_description = (TextView) v.findViewById(R.id.tv_schedule_description);
        tv_attraction_description = (TextView) v.findViewById(R.id.tv_attraction_description);
        tv_event_description = (TextView) v.findViewById(R.id.tv_event_description);

        return v;
    }

    public void RandomResult(){
        ramdom();
        if(ram_event != null){
            Bitmap bm = im.get(ram_event.getImgSrc());
            btn_event_preview.setBackgroundDrawable(new BitmapDrawable(bm));
            btn_event_preview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent1 = new Intent();
                    intent1.setClass(getContext(), ResultActivity.class);
                    Bundle bundle1 = new Bundle();
                    bundle1.putString("keyword", "");
                    bundle1.putInt("category", ResultActivity.CATEGORY_EVENT);
                    intent1.putExtra("data", bundle1);
                    startActivity(intent1);

                    Intent intent = new Intent();
                    Bundle bundle = new Bundle();
                    bundle.putString("name", ram_event.getName());
                    intent.putExtra("data", bundle);
                    intent.setClass(getContext(), DetailActivity.class);
                    startActivity(intent);

                }
            });
            btn_event_preview.setText(ram_event.getName());
        }else{
            event_zone.setVisibility(View.INVISIBLE);
        }
        if(ram_place != null){
            Bitmap bm = im.get(ram_place.getImgSrc());
            btn_attraction_preview.setBackgroundDrawable(new BitmapDrawable(bm));
            btn_attraction_preview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    Bundle bundle = new Bundle();
                    bundle.putString("name", ram_place.getName());
                    intent.putExtra("data", bundle);
                    intent.setClass(getContext(), DetailActivity.class);
                    startActivity(intent);
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.rootFrame, new FragmentSearchTourist()).commit();
                }
            });
            btn_attraction_preview.setText(ram_place.getName());
//            getActivity().setTitle("Search Attraction");
        }else{
            attraction_zone.setVisibility(View.INVISIBLE);
        }
        if(ram_schedule != null){
            ScheduleUtil util = new ScheduleUtil(ram_schedule);
            ArrayList<Place> places = util.getAllPlaceList();

            try{
                Place place = places.get(0);
                Bitmap b = im.get(place.getImgSrc());
                if(b != null){
                    btn_schedule_preview.setBackgroundDrawable(new BitmapDrawable(b));
                }else{
                    btn_schedule_preview.setBackgroundResource(R.drawable.i_love_hk);
                }
                btn_schedule_preview.setText(ram_schedule.getName());
                btn_schedule_preview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent();
                        intent.setClass(getContext(), ScheduleEditActivity.class);
                        intent.putExtra("schedule", ram_schedule);
                        getContext().startActivity(intent);
                    }
                });
            }catch (Exception e){
                btn_schedule_preview.setBackgroundResource(R.drawable.i_love_hk);
                btn_schedule_preview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent();
                        intent.setClass(getContext(), ScheduleEditActivity.class);
                        intent.putExtra("schedule", ram_schedule);
                        getContext().startActivity(intent);
                    }
                });
                btn_schedule_preview.setText(ram_schedule.getName());
            }
        }else{
            btn_schedule_preview.setBackgroundResource(R.drawable.i_love_hk);
            btn_schedule_preview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setClass(getContext(), NewScheduleActivity.class);
                    startActivity(intent);
                }
            });
            btn_schedule_preview.setText("no schedule, go and add one !");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        RandomResult();
    }
}
