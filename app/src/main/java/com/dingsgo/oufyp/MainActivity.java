package com.dingsgo.oufyp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;

import static com.dingsgo.oufyp.R.id.btn_login;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

//    public static final String HOST = "http://192.168.1.219/fyp/index.php?";
    public static final String HOST = "http://fyp.dingsgo.com";
    private String mKeyword;

    private CallbackManager callbackManager;

    public static final String TAG = "MainActivity";
    private AccessToken facebookToken;
    private ProfileTracker profileTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

//        TabLayout mTabLayout = (TabLayout) findViewById(R.id.tabLayout);
//        mTabLayout.addTab(mTabLayout.newTab().setText("TabOne"));//给TabLayout添加Tab
//        mTabLayout.addTab(mTabLayout.newTab().setText("TabTwo"));
//        mTabLayout.addTab(mTabLayout.newTab().setText("TabThree"));
//        mTabLayout.setupWithViewPager(mViewPager);//给TabLayout设置关联ViewPager，如果设置了ViewPager，那么ViewPagerAdapter中的getPageTitle()方法返回的就是Tab上的标题

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        toolbar.setTitle("Hello HK Travel Guide");
        getSupportFragmentManager().beginTransaction().replace(R.id.rootFrame, new HomeFragment()).commit();

//        updataTourists();
//        updataEvents();
//        timerTask();

        factbookLoginSetting();

    }

    private void factbookLoginSetting() {
        callbackManager = CallbackManager.Factory.create();

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                if(currentProfile != null)
                    updateHeader();
                profileTracker.stopTracking();
            }
        };
        profileTracker.startTracking();

        LoginManager.getInstance().registerCallback(
                callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        if(!profileTracker.isTracking())
                            profileTracker.startTracking();
                    }

                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onError(FacebookException exception) {
                    }
                }
        );

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);

        LoginButton btn_login = (LoginButton) header.findViewById(R.id.btn_login);
        btn_login.setReadPermissions("public_profile");

        AccessToken token = AccessToken.getCurrentAccessToken();
        Menu menu = navigationView.getMenu();

        if(token != null){
            btn_login.setVisibility(View.INVISIBLE);
            if(Profile.getCurrentProfile() != null){
                updateHeader();
            }
            menu.findItem(R.id.nav_logout).setVisible(true);
        }else{
            menu.findItem(R.id.nav_logout).setVisible(false);
        }
    }

    private void updateHeader() {
        final Profile profile = Profile.getCurrentProfile();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);
        final LinearLayout l_head_icon = (LinearLayout) header.findViewById(R.id.head_icon);

        Menu menu = navigationView.getMenu();
        menu.findItem(R.id.nav_logout).setVisible(true);

        Button btn_login = (Button) header.findViewById(R.id.btn_login);
        btn_login.setVisibility(View.INVISIBLE);
        TextView tv_username = (TextView) header.findViewById(R.id.tv_username);
        Log.i(TAG, "updateHeader: " + profile.toString());
        tv_username.setText(profile.getName());
        new Thread(){
            @Override
            public void run() {
                try {
                    URL url = new URL("https://graph.facebook.com/"+profile.getId()+"/picture?height=500&width=500");
                    InputStream is = null;
                    is = url.openStream();
                    final Bitmap bm = BitmapFactory.decodeStream(is);
                    l_head_icon.post(new Runnable() {
                        @Override
                        public void run() {
                            l_head_icon.setBackgroundDrawable(new BitmapDrawable(bm));
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void timerTask() {
        SharedPreferences sp = getSharedPreferences("app_preferences", MODE_PRIVATE);
        long record_time = sp.getLong("record_time", 0);
        long curr_time = System.currentTimeMillis();

        long diff = curr_time - record_time;
        Log.i(TAG, "timerTask: " + diff / 1000 + " s");
        // 1 hour
        if(curr_time - record_time > 1000 * 60 * 60){
            SharedPreferences.Editor ed = sp.edit();
            ed.putLong("record_time", curr_time);
            ed.commit();
            updataTourists();
            updataEvents();
        }
    }

    private void updataTourists() {
        Log.i(TAG, "updataTourists: update tourists list.");
        TouristManager touristManage = new TouristManager(this);
        TouristManager.GetAllTouristTask conn = touristManage.new GetAllTouristTask(this);
        conn.execute(HOST + "/api/tourist/all");
//        conn.execute("http://192.168.1.219/fyp/index.php?/api/tourist/all");
    }

    private void updataEvents(){
        EventManager eventManager = new EventManager(this);
        EventManager.GetAllEventTask econn = eventManager.new GetAllEventTask(this);
        econn.execute(HOST + "/api/event/all");
//        econn.execute("http://192.168.1.219/fyp/index.php?/api/event/all");
    }

    public static boolean isMobileNetworkAvailable(Context con){
        ConnectivityManager connMgr = null;
        if(null == connMgr){
            connMgr = (ConnectivityManager)con.getSystemService(Context.CONNECTIVITY_SERVICE);
        }
        NetworkInfo wifiInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobileInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if(wifiInfo.isAvailable()){
            return true;
        }else if(mobileInfo.isAvailable()){
            return true;
        }else{
            return false;
        }
    }
    public static void showConnectionNADialog(final Context con){
        AlertDialog.Builder builder = new AlertDialog.Builder(con);
        builder.setTitle("Network not available")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage("Go and check Wireless & networks settings?")
                .setPositiveButton("OK", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                        con.startActivity(intent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setCancelable(true);

        builder.create().show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        switch (id){
            case R.id.nav_logout:
                facebookLogout();
                return true;
        }

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment fragment = null;
        if (id == R.id.action_home) {
            fragment = new HomeFragment();
            setTitle("Hello HK Travel Guide");
        } else if (id == R.id.action_search) {
            fragment = FragmentSearchTourist.newInstance("search","..");
            setTitle("Search Attraction");
        } else if (id == R.id.action_schedule) {
            Intent intent = new Intent();
            fragment = new FragmentSchedule();
            setTitle("Schedule");
        }else if(id == R.id.action_event){
            Intent intent = new Intent();
            intent.setClass(this, ResultActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("keyword", "");
            bundle.putInt("category", ResultActivity.CATEGORY_EVENT);
            intent.putExtra("data", bundle);
            startActivity(intent);
            return true;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        ft.replace(R.id.rootFrame, fragment);
        ft.commit();

        return true;
    }

    private void facebookLogout() {
        Log.i(TAG, "facebookLogout: ");
        LoginManager.getInstance().logOut();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);
        final LinearLayout l_head_icon = (LinearLayout) header.findViewById(R.id.head_icon);
        Menu menu = navigationView.getMenu();
        menu.findItem(R.id.nav_logout).setVisible(false);

        LoginButton btn_login = (LoginButton) header.findViewById(R.id.btn_login);
        btn_login.setVisibility(View.VISIBLE);
        TextView tv_username = (TextView) header.findViewById(R.id.tv_username);
        tv_username.setText("");

        l_head_icon.setBackgroundResource(R.drawable.side_nav_bar);
    }

    @Override
    protected void onDestroy() {
        if(profileTracker.isTracking())
            profileTracker.stopTracking();
        super.onDestroy();
    }
}
