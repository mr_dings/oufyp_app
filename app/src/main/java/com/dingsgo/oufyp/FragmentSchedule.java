package com.dingsgo.oufyp;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dingsgo.oufyp.scheduleProcess.NewScheduleActivity;
import com.dingsgo.oufyp.scheduleProcess.ScheduleEditActivity;
import com.dingsgo.oufyp.scheduleProcess.ScheduleTrackingMap;
import com.google.android.gms.maps.model.BitmapDescriptor;

import org.w3c.dom.Text;

import java.io.IOException;
import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class  FragmentSchedule extends Fragment {

    private ScheduleAdapter adapter;

    public FragmentSchedule() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = View.inflate(getContext(), R.layout.fragment_schedule, null);

        RecyclerView rv = (RecyclerView) v.findViewById(R.id.recyclerView);
        rv.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new ScheduleAdapter(getContext());
        rv.setAdapter(adapter);
        setHasOptionsMenu(true);
        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_add, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_add:
//                showSingleChoiceDialog();
                Intent intent = new Intent();
                intent.setClass(getContext(), NewScheduleActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showSingleChoiceDialog() {
        AlertDialog.Builder builder=new AlertDialog.Builder(getContext());
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setTitle("choice");

        /**
         * 设置内容区域为单选列表项
         */
        final String[] items={"auto","custom"};
        builder.setSingleChoiceItems(items, 1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(getContext(), "You clicked "+items[i], Toast.LENGTH_SHORT).show();
            }
        });

        builder.setCancelable(true);
        AlertDialog dialog=builder.create();
        dialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter.refresh();
    }
}

class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.mViewholder>{

    private Context context;
    private ArrayList<Schedule> mList;
    private ImageManager im;

    ScheduleAdapter(Context context){
        this.context = context;
        mList = new ArrayList<>();
        try {
            mList = Schedule.loadFromLocalStorage(context);
        } catch (IOException e) {
            e.printStackTrace();
        }
        im = new ImageManager(context);
    }

    public void refresh(){
        try {
            mList = Schedule.loadFromLocalStorage(context);
            notifyDataSetChanged();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public mViewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = View.inflate(context, R.layout.view_rv_item, null);
        return new mViewholder(v);
    }

    @Override
    public void onBindViewHolder(mViewholder holder, final int position) {
        Schedule curr_schedule = mList.get(position);
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        holder.tv_date.setText(formatter.format(new Date(curr_schedule.getStartDate())));
        holder.iv_first_ic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(context, ScheduleEditActivity.class);
                intent.putExtra("schedule", mList.get(position));
                context.startActivity(intent);
            }
        });
        holder.btn_detele.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Schedule schedule = mList.remove(position);
                Schedule.deleteFromLocalStorage(context, schedule.getName());
                notifyDataSetChanged();
            }
        });
        holder.btn_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(context, ScheduleTrackingMap.class);
                intent.putExtra("schedule", mList.get(position));
                context.startActivity(intent);
//                Schedule schedule = mList.get(position);
//                ScheduleUtil util = new ScheduleUtil(schedule);
//                util.printSchedule();
//                util.bestSchedule();
            }
        });
        ScheduleUtil util = new ScheduleUtil(curr_schedule);
        holder.iv_first_ic.setText("  " + curr_schedule.getName());
        ArrayList<Place> places = util.getAllPlaceList();
        try{
            Place place = places.get(0);
            Bitmap b = im.get(place.getImgSrc());
            if(b != null){
                holder.iv_first_ic.setBackgroundDrawable(new BitmapDrawable(b));
            }else{
                holder.iv_first_ic.setBackgroundResource(R.drawable.i_love_hk);
            }
        }catch (Exception e){
            holder.iv_first_ic.setBackgroundResource(R.drawable.i_love_hk);
        }
    }


    @Override
    public int getItemCount() {
        return mList.size();
    }


    public static class mViewholder extends RecyclerView.ViewHolder {

        TextView tv_date;
        Button btn_detele, btn_test;
        Button iv_first_ic;

        public mViewholder(View itemView) {
            super(itemView);
            tv_date = (TextView) itemView.findViewById(R.id.tv_date);
            btn_detele = (Button) itemView.findViewById(R.id.item_delete);
            btn_test = (Button) itemView.findViewById(R.id.item_track);
            iv_first_ic = (Button) itemView.findViewById(R.id.iv_first_ic);
        }
    }
}
