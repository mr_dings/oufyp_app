package com.dingsgo.oufyp;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by dings on 15/4/2017.
 */

public class MapPlaceInfo {
    private String name;
    private String type;
    private LatLng position;
    private String address;

    private MapPlaceInfo(){

    }

    public MapPlaceInfo(String name, String type, LatLng position, String address) {
        this.name = name;
        this.type = type;
        this.position = position;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public LatLng getPosition() {
        return position;
    }

    public void setPosition(LatLng position) {
        this.position = position;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public static MapPlaceInfo parseJSON(JSONObject obj) throws JSONException {
        MapPlaceInfo info = new MapPlaceInfo();
        info.setName(obj.getString("name"));
        info.setType(obj.getString("type"));
        info.setAddress(obj.getString("address"));
        JSONObject loc = obj.getJSONObject("loc");
        JSONArray coord = loc.getJSONArray("coordinates");
        LatLng ll = new LatLng(coord.getDouble(1), coord.getDouble(0));
        info.setPosition(ll);
        return info;
    }

    @Override
    public String toString() {
        return "MapPlaceInfo{" +
                "name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", position=" + position +
                ", address='" + address + '\'' +
                '}';
    }
}
