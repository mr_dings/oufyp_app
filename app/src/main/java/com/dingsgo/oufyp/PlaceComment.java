package com.dingsgo.oufyp;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by dings on 16/4/2017.
 */
public class PlaceComment {

    private String place_name;
    private String body;
    private long create_at;
    private String author;

    public PlaceComment(){

    }

    public PlaceComment(String place_name, String body, long create_at, String author) {
        this.place_name = place_name;
        this.body = body;
        this.create_at = create_at;
        this.author = author;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public long getCreate_at() {
        return create_at;
    }

    public void setCreate_at(long create_at) {
        this.create_at = create_at;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPlace_name() {
        return place_name;
    }

    public void setPlace_name(String place_name) {
        this.place_name = place_name;
    }

    public JSONObject toJson() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("place_name", place_name);
        jsonObject.put("body", body);
        jsonObject.put("create_at", create_at);
        jsonObject.put("create_by", author);
        return jsonObject;
    }

    public static PlaceComment parseJSON(JSONObject object) throws JSONException {
        PlaceComment pc = new PlaceComment();
        pc.place_name = object.getString("place_name");
        pc.body = object.getString("body");
        pc.create_at = object.getLong("create_at");
        pc.author = object.getString("create_by");
        return pc;
    }

    @Override
    public String toString() {
        return "PlaceComment{" +
                "place_name='" + place_name + '\'' +
                ", body='" + body + '\'' +
                ", create_at=" + create_at +
                ", author='" + author + '\'' +
                '}';
    }
}
