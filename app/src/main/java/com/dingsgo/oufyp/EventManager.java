package com.dingsgo.oufyp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dings on 31/1/2017.
 */

public class EventManager implements DBManager<Event>{
    private Context context;
    private static final String TAG = "EventManager";
    private FypDB db;


    public EventManager(Context context) {
        this.context = context;
        this.db = new FypDB(context);
    }

    @Override
    public long insert(Event event) {
        SQLiteDatabase db = this.db.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put("name", event.name);
        cv.put("address", event.address);
        cv.put("description", event.description);
        cv.put("detail", event.detail);
        cv.put("website", event.website);
        cv.put("lat", event.lat);
        cv.put("lon", event.lon);
        cv.put("tel", event.tel);
        cv.put("imgSrc", event.imgSrc);
        cv.put("startDate", event.startDate);
        cv.put("endDate", event.endDate);
        cv.put("openingHoursDescription", event.openingHoursDescription);
        cv.put("admission", event.admission);
        cv.put("openHour", event.openHour);
        cv.put("endHour", event.endHour);
        cv.put("recommendMinute", event.recommendMinute);

        long row = db.insert(FypDB.EVENT_TABLE, null, cv);
        db.close();
        return row;
    }

    @Override
    public ArrayList<Event> getAll() {
        SQLiteDatabase db = this.db.getReadableDatabase();
        Cursor cursor = db
                .query(FypDB.EVENT_TABLE, null, null, null, null, null, null);
        ArrayList<Event> events = new ArrayList<Event>();
        while (cursor.moveToNext()) {
            Event event = getRowEvent(cursor);
            events.add(event);
        }
        db.close();
        return events;
    }

    public Event getRamdomEvent(){
        SQLiteDatabase db = this.db.getReadableDatabase();
        Cursor cursor = db
                .query(FypDB.EVENT_TABLE, null, null, null, null, null, "random()", "1");

        if(cursor.moveToNext()){
            Event event = getRowEvent(cursor);
            return event;
        }
        return null;
    }

    private void removeAll(){
        SQLiteDatabase db = this.db.getWritableDatabase();
        db.delete(FypDB.EVENT_TABLE, null, null);
    }

    private Event getRowEvent(Cursor cursor) {
        Event event = new Event();
        event.name = cursor.getString(cursor.getColumnIndex("name"));
        event.address = cursor.getString(cursor.getColumnIndex("address"));
        event.description = cursor.getString(cursor.getColumnIndex("description"));
        event.detail = cursor.getString(cursor.getColumnIndex("detail"));
        event.website = cursor.getString(cursor.getColumnIndex("website"));
        event.lat = cursor.getDouble(cursor.getColumnIndex("lat"));
        event.lon = cursor.getDouble(cursor.getColumnIndex("lon"));
        event.tel = cursor.getString(cursor.getColumnIndex("tel"));
        event.imgSrc = cursor.getString(cursor.getColumnIndex("imgSrc"));
        event.startDate = cursor.getLong(cursor.getColumnIndex("startDate"));
        event.endDate = cursor.getLong(cursor.getColumnIndex("endDate"));
        event.openingHoursDescription = cursor.getString(cursor.getColumnIndex("openingHoursDescription"));
        event.admission = cursor.getString(cursor.getColumnIndex("admission"));
        event.openHour = cursor.getInt(cursor.getColumnIndex("openHour"));
        event.endHour = cursor.getInt(cursor.getColumnIndex("endHour"));
        event.recommendMinute = cursor.getInt(cursor.getColumnIndex("recommendMinute"));
        return event;
    }

    @Override
    public Event get(String name) {
        SQLiteDatabase db = this.db.getReadableDatabase();
        Cursor cursor = db
                .query(FypDB.EVENT_TABLE, null, "name = ?", new String[]{name}, null, null, null);
        Event event = null;
        if (cursor.moveToNext()) {
            event = getRowEvent(cursor);
        }
        return event;
    }

    @Override
    public ArrayList<Event> query(String keyword) {
        String sql = "select * from " + FypDB.EVENT_TABLE + " where name like ?";
        SQLiteDatabase db = this.db.getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, new String[]{"%" + keyword + "%"});
        ArrayList<Event> events = new ArrayList<Event>();
        while (cursor.moveToNext()) {
            Event event = getRowEvent(cursor);
            events.add(event);
        }
        db.close();
        return events;
    }

    public ArrayList<Event> getCompleteEvents(){
        Log.i(TAG, "getCompleteEvents: ");
        String sql = "select * from " + FypDB.EVENT_TABLE + " where lat != 0 and lon != 0 and startDate > 0 and openHour > 0 and recommendMinute <= 300";
        SQLiteDatabase db = this.db.getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        ArrayList<Event> events = new ArrayList<Event>();
        while (cursor.moveToNext()) {
            Event event = getRowEvent(cursor);
            Log.i(TAG, "getCompleteEvents: " + event.toString());
            events.add(event);
        }
        db.close();
        return events;
    }

    public ArrayList<Event> getEventsByDayTime(long start_day, long end_day){
        Log.i(TAG, "getCompleteEvents: ");
        String sql = "select * from " + FypDB.EVENT_TABLE + " where lat != 0 and lon != 0 and " +
                "((startDate <= " + start_day + " and endDate >= "+start_day+") or (startDate <= " + end_day + " and endDate >= "+end_day+") or (startDate <= " + start_day + " and endDate >= "+end_day+"))" +
                " and openHour > 0 and recommendMinute <= 300";
        SQLiteDatabase db = this.db.getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        ArrayList<Event> events = new ArrayList<Event>();
        while (cursor.moveToNext()) {
            Event event = getRowEvent(cursor);
            Log.i(TAG, "getCompleteEvents: " + event.toString());
            events.add(event);
        }
        db.close();
        return events;
    }

    public ArrayList<Event> exist(Place place){
        String sql = "select * from " + FypDB.EVENT_TABLE + " where name not in ( ? )";
        SQLiteDatabase db = this.db.getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, new String[]{place.name});
        ArrayList<Event> events = new ArrayList<Event>();
        while (cursor.moveToNext()) {
            Event e = getRowEvent(cursor);
            events.add(e);
        }
        db.close();
        return events;
    }

    public class GetAllEventTask extends AsyncTask<String, Integer, List<Event>> {

        public static final String TAG = "GetAllEventTask";

        private Context context;

        public GetAllEventTask(Context context) {
            this.context = context;
        }

        //onPreExecute方法用于在执行后台任务前做一些UI操作
        @Override
        protected void onPreExecute() {
            Log.i(TAG, "onPreExecute() called");

        }

        //doInBackground方法内部执行后台任务,不可在此方法内修改UI
        @Override
        protected List<Event> doInBackground(String... params) {
            //用POST发送JSON数据
            String data = getURlResponse(params[0]);
            try {
                JSONObject json = new JSONObject(data);
                JSONArray tourists = json.getJSONArray("events");
                ArrayList<Event> list = new ArrayList<Event>();
                for (int i = 0; i < tourists.length(); i++) {
                    JSONObject JSONTourist = tourists.getJSONObject(i);
                    Event event = Event.parseJSON(JSONTourist);
                    list.add(event);
                    Log.i(TAG, "doInBackground: tourst -> " + event.toString());
                }
                return list;
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Event> list) {
            if (list != null) {
                // success action
                EventManager tm = new EventManager(context);
                tm.removeAll();
                for (Event event : list) {
                    tm.insert(event);
                }
            }
            // fail action
        }

        private String getURlResponse(String s) {
            HttpURLConnection httpURLConnection = null;
            InputStream inputStream = null;
            String resultData = "";

            try {
                URL url = new URL(s);   //URl对象
                Log.i(TAG, "getURlResponse: url = " + url);
                httpURLConnection = (HttpURLConnection) url.openConnection();   //使用URl打开一个链接
                httpURLConnection.setDoInput(true);//允许输入流，即允许下载
                httpURLConnection.setDoOutput(true);//允许输出流，即允许上传
                httpURLConnection.setUseCaches(false); //不使用缓冲
                httpURLConnection.setRequestMethod("GET"); //使用get请求

                int responseCode = httpURLConnection.getResponseCode();
                Log.i(TAG, "getURlResponse: response code = " + responseCode);

                inputStream = httpURLConnection.getInputStream();//获取输入流，此时才真正建立链接
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String inputLine;
                while ((inputLine = bufferedReader.readLine()) != null) {
                    resultData = inputLine + resultData + "\n";
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (inputStream != null)
                        inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                httpURLConnection.disconnect();
            }
            return resultData;
        }
    }

}
