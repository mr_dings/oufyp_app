package com.dingsgo.oufyp;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by dings on 31/1/2017.
 */

public class Event extends Place implements Serializable {
    private static final long serialVersionUID = 9832905821532L;
    private static final String TAG = "Event";

//    public long startDate;
//    public long endDate;
//    public String openingHoursDescription;
//    public String admission;


    public static Event parseJSON(JSONObject object) {
        try {
            Event event = new Event();
            event.name = object.getString("name");
            event.address = object.getString("address");
            event.description = object.getString("description");
            event.detail = object.getString("detail");
            event.website = object.getString("website");
            event.tel = object.getString("tel");
            event.imgSrc = object.getString("imgSrc");
            event.lat = object.getDouble("lat");
            event.lon = object.getDouble("lon");
            event.openHour = object.getInt("openHour");
            event.endHour = object.getInt("endHour");
            event.recommendMinute = object.getInt("recommendMinute");

            if(object.isNull("startDate"))
                event.startDate = -1;
            else
                event.startDate = object.getLong("startDate");
            if(object.isNull("endDate"))
                event.endDate = -1;
            else
                event.endDate = object.getLong("endDate");
            event.openingHoursDescription = object.getString("openingHoursDescription");
            event.admission = object.getString("admission");
            return event;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String toString() {
        return "Event{" +
                "startDate=" + startDate +
                ", endDate=" + endDate +
                ", openingHoursDescription='" + openingHoursDescription + '\'' +
                ", admission='" + admission + '\'' +
                "place=" + super.toString() +
                '}';
    }
}
