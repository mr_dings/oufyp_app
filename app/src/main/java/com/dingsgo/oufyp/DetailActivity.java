package com.dingsgo.oufyp;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.DrawableRes;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatDrawableManager;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookDialog;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.share.ShareApi;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class DetailActivity extends AppCompatActivity {

    private static final String TAG = "DetailActivity";

    private int detailCategory;
    private String title;
    private Place place;
    private boolean hasLocation = true;
    private ImageView iv_header;

    private CallbackManager callbackManager;
    private LoginManager loginManager;
    private ImageManager imageManger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setOnMenuItemClickListener(onMenuItemClick);
//        toolbar.setNavigationIcon(android.R.drawable.ic_menu_revert);
        toolbar.setNavigationIcon(AppCompatDrawableManager.get().getDrawable(this, R.drawable.ic_arrow_back_white_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                DetailActivity.this.finish();
            }
        });

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        handleIntent();

        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbarLayout);
        collapsingToolbarLayout.setTitle(title);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabBtn);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
        new loadNearbyEvent().execute();
        new loadNearbyTourist().execute();
        new GetCommentTask().execute(place.name);
    }

    private void handleUI(Place place) {
        imageManger = new ImageManager(this);

        TextView tv_detail = (TextView) findViewById(R.id.tv_detail);
        TextView tv_category = (TextView) findViewById(R.id.tv_category);
        TextView tv_date = (TextView) findViewById(R.id.tv_date);
        TextView tv_address = (TextView) findViewById(R.id.tv_address);
        TextView tv_openHour = (TextView) findViewById(R.id.tv_openHour);
        TextView tv_recommend = (TextView) findViewById(R.id.tv_recommend);
        TextView tv_tel = (TextView) findViewById(R.id.tv_tel);
        iv_header = (ImageView) findViewById(R.id.iv_header);
        TextView tv_website = (TextView) findViewById(R.id.tv_website);
        tv_tel.setClickable(true);
        tv_website.setClickable(true);
        tv_tel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(DetailActivity.this, "tv tel click", Toast.LENGTH_SHORT).show();
            }
        });
        tv_website.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(DetailActivity.this, "tv website click", Toast.LENGTH_SHORT).show();
            }
        });

        if(place instanceof Tourist) {
            Tourist tourist = (Tourist) place;
            tv_category.setText(tourist.tag1 + " " + tourist.tag2 + " " + tourist.tag3);
        }else if (place instanceof Event){
            tv_category.setText("event");
            long startTime = ((Event) place).startDate;
            long endTime = ((Event) place).endDate;
            Date startDate = new Date(startTime);
            Date endDate = new Date(endTime);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String startTimeString = sdf.format(startDate);
            String endTimeString = sdf.format(endDate);

            if(startTime < 0){
                startTimeString = "xxxx-xx-xx";
            }
            if(endTime < 0){
                endTimeString = "xxxx-xx-xx";
            }

            tv_date.setText(startTimeString + " - " + endTimeString);
        }

        tv_detail.setText(place.detail);

        tv_address.setText(place.address);
        tv_tel.setText(place.tel);
        tv_website.setText(place.website);
        int openHour = place.openHour < 0 ? 0 : place.openHour;
        int endHour = place.endHour < 0 ? 2400 : place.endHour;
        tv_openHour.setText(openHour + " - " + endHour);
        tv_recommend.setText(place.recommendMinute + "");
        imageManger.display(iv_header, place.imgSrc);
    }

    private void handleIntent() {
        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("data");
        String name = bundle.getString("name");
        int category = bundle.getInt("category", -1);
        if(name.isEmpty()){
            throw new RuntimeException("name can not be empty");
        }
        this.title = name;
        Log.i(TAG, "handleIntent: category = " + category);
        Log.i(TAG, "handleIntent: name = " + name);
        DBManager manager = new TouristManager(this);
        Place place = (Place) manager.get(name);
        if(place == null){
            manager = new EventManager(this);
            place = (Place) manager.get(name);
        }
        if (place == null)
            throw new RuntimeException("no such tourist name " + name);
        this.detailCategory = category;
        this.place = place;
        this.hasLocation = !(place.lat == 0 && place.lon == 0);
        Log.i(TAG, "handleIntent: " + place.toString());
        handleUI(place);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        if(hasLocation){
            getMenuInflater().inflate(R.menu.menu_map, menu);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    private Toolbar.OnMenuItemClickListener onMenuItemClick = new Toolbar.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            String msg = "";
            switch (menuItem.getItemId()) {
                case R.id.action_map:
                    Intent intent = new Intent();
                    intent.setClass(getApplicationContext(), MapActivity.class);
                    ArrayList<Place> list = new ArrayList<Place>();
                    list.add(place);
                    intent.putExtra("places", list);
                    intent.putExtra("category", detailCategory);
                    startActivity(intent);
                    break;
                case R.id.action_share:

                    SharePhoto photo = new SharePhoto.Builder()
                            .setBitmap(imageManger.get(place.imgSrc))
                            .setCaption(place.getDetail())
                            .build();

                    SharePhotoContent content = new SharePhotoContent.Builder()
                            .addPhoto(photo)
                            .build();

                    ShareApi.share(content, null);
                    ShareDialog shareDialog = new ShareDialog(DetailActivity.this);
                    if(shareDialog.canShow(SharePhotoContent.class)) {
                        shareDialog.show(DetailActivity.this, content);
                    }

//                    ShareLinkContent linkContent = new ShareLinkContent.Builder()
//                            .setContentUrl(Uri.parse("http://www.baidu.com"))
//                            .setContentTitle("facebook share")
//                            .setContentDescription("good description")
//                            .build();
//                    shareDialog.show(linkContent);
                    break;
            }
            return true;
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent data)
    {
        super.onActivityResult(requestCode, responseCode, data);
        callbackManager.onActivityResult(requestCode, responseCode, data);
    }

    private ArrayList<Tourist> nearbyTouristList;
    private ArrayList<Event> nearbyEventList;

    class loadNearbyTourist extends AsyncTask<String, Integer, Boolean>{

        @Override
        protected Boolean doInBackground(String... params) {
            nearbyTouristList = new ArrayList<>();
            TouristManager tm = new TouristManager(getApplicationContext());
            ArrayList<Tourist> tourists = tm.exist(place);
            for(Tourist tourist : tourists){
//                Log.i(TAG, "doInBackground: " + tourist.toString());
                double distance = Tourist.getDistance(tourist, place);
                if(distance < 2000)
                    nearbyTouristList.add(tourist);
            }
            if(nearbyTouristList.size() > 0)
                return true;
            return false;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            ProgressBar pb = (ProgressBar) findViewById(R.id.pb_nearby_tourist);
            Button btn_nearby_tourist = (Button) findViewById(R.id.btn_nearby_tourist);
            pb.setVisibility(View.GONE);
            if(!success) {
                btn_nearby_tourist.setVisibility(View.GONE);
                return;
            }
            btn_nearby_tourist.setVisibility(View.VISIBLE);
            btn_nearby_tourist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setClass(getApplicationContext(), ResultActivity.class);
                    Bundle data = new Bundle();
                    data.putInt("mode", ResultActivity.MODE_NEARBY);
                    data.putString("keyword","nearby tourists");
                    data.putInt("category", ResultActivity.CATEGORY_TOURIST);
                    data.putSerializable("list", nearbyTouristList);
                    intent.putExtra("data", data);
                    startActivity(intent);
                }
            });
        }
    }
    class loadNearbyEvent extends AsyncTask<String, Integer, Boolean>{

        @Override
        protected Boolean doInBackground(String... params) {
            nearbyEventList = new ArrayList<>();
            EventManager em = new EventManager(getApplicationContext());
            ArrayList<Event> eventList = em.exist(place);
            for(Event event : eventList){
//                Log.i(TAG, "doInBackground: " + event.toString());
                double distance = Tourist.getDistance(event, place);
                if(distance < 2000)
                    nearbyEventList.add(event);
            }
            if(nearbyEventList.size() > 0)
                return true;
            return false;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            ProgressBar pb = (ProgressBar) findViewById(R.id.pb_nearby_event);
            Button btn_nearby_event = (Button) findViewById(R.id.btn_nearby_event);
            pb.setVisibility(View.GONE);
            if(!success) {
                btn_nearby_event.setVisibility(View.GONE);
                return;
            }
            btn_nearby_event.setVisibility(View.VISIBLE);
            btn_nearby_event.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setClass(getApplicationContext(), ResultActivity.class);
                    Bundle data = new Bundle();
                    data.putInt("mode", ResultActivity.MODE_NEARBY);
                    data.putString("keyword","nearby events");
                    data.putInt("category", ResultActivity.CATEGORY_EVENT);
                    data.putSerializable("list", nearbyEventList);
                    intent.putExtra("data", data);
                    startActivity(intent);
                }
            });
        }
    }

    class GetCommentTask extends AsyncTask<String, Void, List<PlaceComment>> {

        @Override
        protected List<PlaceComment> doInBackground(String... place_name) {
            String data = getURlResponse(MainActivity.HOST + "/api/getComment/" + URLEncoder.encode(place_name[0]));
            try {
                JSONObject json = new JSONObject(data);
                JSONArray comments = json.getJSONArray("comments");
                ArrayList<PlaceComment> list = new ArrayList<PlaceComment>();
                for (int i = 0; i < comments.length(); i++) {
                    JSONObject JSONTourist = comments.getJSONObject(i);
                    PlaceComment pc = PlaceComment.parseJSON(JSONTourist);
                    list.add(pc);
                    Log.i(TAG, "doInBackground: " + pc.toString());
                }
                return list;
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return new ArrayList<PlaceComment>();
        }

        @Override
        protected void onPostExecute(List<PlaceComment> placeComments) {
            LinearLayout comment_zone = (LinearLayout) findViewById(R.id.comment_zone);
            Date date = new Date();
            for(PlaceComment pc : placeComments){
                View comment_view = View.inflate(DetailActivity.this, R.layout.comment_item, null);
                TextView body = (TextView) comment_view.findViewById(R.id.body);
                TextView name = (TextView) comment_view.findViewById(R.id.name);
                TextView time = (TextView) comment_view.findViewById(R.id.time);
                body.setText(pc.getBody());
                name.setText(pc.getAuthor());
                date.setTime(pc.getCreate_at());
                time.setText(DateFormat.format("yyyy-MM-dd, kk:mm", date));

                comment_zone.addView(comment_view,0);
                comment_zone.invalidate();
                Log.i(TAG, "onPostExecute: " + pc.toString());
            }
        }
    }

    class InsertCommentTask extends AsyncTask<PlaceComment, Void, Boolean>{

        private PlaceComment pc;
        @Override
        protected Boolean doInBackground(PlaceComment... params) {
            try {
                String res = makeRequest(MainActivity.HOST + "/api/insertComment", params[0].toJson().toString());
                Log.i(TAG, "doInBackground: " + res);

                this.pc = params[0];
                return true;

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
//            if(aBoolean == true && this.pc != null) {
                LinearLayout comment_zone = (LinearLayout) findViewById(R.id.comment_zone);
                Date date = new Date();
                View comment_view = View.inflate(DetailActivity.this, R.layout.comment_item, null);
                TextView body = (TextView) comment_view.findViewById(R.id.body);
                TextView name = (TextView) comment_view.findViewById(R.id.name);
                TextView time = (TextView) comment_view.findViewById(R.id.time);
                body.setText(pc.getBody());
                name.setText(pc.getAuthor());
                date.setTime(pc.getCreate_at());
                time.setText(DateFormat.format("yyyy-MM-dd, kk:mm", date));
                comment_zone.addView(comment_view);
//            }
        }
    }

    public static String getURlResponse(String s) {
        HttpURLConnection httpURLConnection = null;
        InputStream inputStream = null;
        String resultData = "";

        try {
            URL url = new URL(s);   //URl对象
            Log.i(TAG, "getURlResponse: url = " + url);
            httpURLConnection = (HttpURLConnection) url.openConnection();   //使用URl打开一个链接
            httpURLConnection.setDoInput(true);//允许输入流，即允许下载
            httpURLConnection.setDoOutput(true);//允许输出流，即允许上传
            httpURLConnection.setUseCaches(false); //不使用缓冲
            httpURLConnection.setRequestMethod("GET"); //使用get请求

            int responseCode = httpURLConnection.getResponseCode();
            Log.i(TAG, "getURlResponse: response code = " + responseCode);

            inputStream = httpURLConnection.getInputStream();//获取输入流，此时才真正建立链接
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String inputLine;
            while ((inputLine = bufferedReader.readLine()) != null) {
                resultData = inputLine + resultData + "\n";
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputStream != null)
                    inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            httpURLConnection.disconnect();
        }
        return resultData;
    }

    public static String makeRequest(String uri, String json) {
        HttpPost postRequest = new HttpPost(uri);
        try {
            StringEntity entity = new StringEntity(json, "UTF-8");
            entity.setContentEncoding("UTF-8");
            entity.setContentType("application/json");
            HttpClient client = new DefaultHttpClient();
            postRequest.setEntity(entity);
            HttpResponse response = client.execute(postRequest);
            int code = response.getStatusLine().getStatusCode();
//            Log.i(TAG, "makeRequest: response code -> " +code);
            if(code == 200){
                HttpEntity res_entity = response.getEntity();
//                String responseString = EntityUtils.toString(res_entity,"UTF-8");
//                return responseString;
                return EntityUtils.toString(res_entity);

            }else{
//                Log.i(TAG, "makeRequest: ..?");
            }

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void addComment(View view){
        final EditText inputServer = new EditText(this);
        inputServer.setFocusable(true);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("add comment").setIcon(
                android.R.drawable.ic_input_add).setView(inputServer)
                .setNegativeButton("cancel", null);
        builder.setPositiveButton("add",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String input = inputServer.getText().toString();
                        if(input.isEmpty()){
                            return;
                        }else{
                            InsertCommentTask insertComment = new InsertCommentTask();
                            PlaceComment pc = new PlaceComment();
                            pc.setPlace_name(place.name);
                            pc.setBody(input);
                            pc.setCreate_at(System.currentTimeMillis());
                            pc.setAuthor("過路人");
                            insertComment.execute(pc);
                        }
                    }
                });
        builder.show();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
