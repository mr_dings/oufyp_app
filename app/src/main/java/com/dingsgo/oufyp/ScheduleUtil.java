package com.dingsgo.oufyp;

import android.util.Log;

import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.ListIterator;
import java.util.Map;

/**
 * Created by dings on 9/2/2017.
 */

public class ScheduleUtil {

    private static final String TAG = "ScheduleUtil";
    private Schedule mSchedule;
    private ArrayList<Place> allPlaceList;

    double walk_speed = 45;

    public ScheduleUtil(Schedule schedule){
        this.mSchedule = schedule;
        this.allPlaceList = new ArrayList<>();
        for(int i = 1; i <= schedule.getDay_period(); i++){
            Schedule.DaySchedule ds = schedule.getSchedule(i);
            for(int j = 0 ; j < 3; j++){
                ArrayList<Place> time_schedule = ds.getTimeSchedule(j);
                this.allPlaceList.addAll(time_schedule);
            }
        }
    }

    public ArrayList<Place> getAllPlaceList(){
        return this.allPlaceList;
    }

    private ArrayList<Place> morningList = new ArrayList<>();
    private ArrayList<Place> afternoonList = new ArrayList<>();
    private ArrayList<Place> nightList = new ArrayList<>();
    private ArrayList<Place> allTimeList = new ArrayList<>();

    private ArrayList<Place> sortByDistanceList = new ArrayList<>();

    public Map<Integer, ArrayList<Place>> getDayPlace(){
        Map<Integer, ArrayList<Place>> map = new HashMap<>();
        for(int i = 1; i <= mSchedule.getDay_period(); i++){
            map.put(i, mSchedule.getSchedule(i).getPlaceList());
        }
        return map;
    }

    private Schedule.DaySchedule bestSchedule(Schedule.DaySchedule daySchedule){
        ArrayList<Place> list = sortByDistance(daySchedule.getPlaceList());

        ArrayList<Place> morningList = new ArrayList<>();
        ArrayList<Place> afternoonList = new ArrayList<>();
        ArrayList<Place> nightList = new ArrayList<>();
        ArrayList<Place> notNight = new ArrayList<>();
        ArrayList<Place> notmoringList = new ArrayList<>();
        ArrayList<Place> allTimeList = new ArrayList<>();

        ArrayList<Place> resultList = new ArrayList<>();

        for(Place p: list){
            if(p.getOpenHour() >= 800 && p.getEndDate() < 1200){
                morningList.add(p);
            }else if(p.getOpenHour() >= 1200 && p.getEndDate() < 1800){
                afternoonList.add(p);
            }else if(p.getOpenHour() >= 1800 && p.getEndDate() < 2400){
                afternoonList.add(p);
            }else if(p.getOpenHour() >= 800 && p.getEndDate() < 1800){
                notNight.add(p);
            }else if(p.getOpenHour() >= 1200 && p.getEndDate() < 2400){
                notmoringList.add(p);
            }else{
                allTimeList.add(p);
            }
        }

        ArrayList<Place> target = afternoonList;
        double distance = Double.MAX_VALUE;

        for(Place p: notNight){
            for (int i = 0; i<morningList.size(); i++) {
                double temp = Place.getDistance(resultList.get(i), p);
                if (distance > temp) {
                    distance = temp;
                    target = morningList;
                }
            }
            for (int i = 0; i<afternoonList.size(); i++) {
                double temp = Place.getDistance(resultList.get(i), p);
                if (distance > temp) {
                    distance = temp;
                    target = afternoonList;
                }
            }
            target.add(p);
        }

        for(Place p: notmoringList){
            for (int i = 0; i<afternoonList.size(); i++) {
                double temp = Place.getDistance(resultList.get(i), p);
                if (distance > temp) {
                    distance = temp;
                    target = afternoonList;
                }
            }
            for (int i = 0; i<nightList.size(); i++) {
                double temp = Place.getDistance(resultList.get(i), p);
                if (distance > temp) {
                    distance = temp;
                    target = nightList;
                }
            }
            target.add(p);
        }

        for(Place p: allPlaceList){
            for (int i = 0; i<morningList.size(); i++) {
                double temp = Place.getDistance(resultList.get(i), p);
                if (distance > temp) {
                    distance = temp;
                    target = morningList;
                }
            }
            for (int i = 0; i<afternoonList.size(); i++) {
                double temp = Place.getDistance(resultList.get(i), p);
                if (distance > temp) {
                    distance = temp;
                    target = afternoonList;
                }
            }
            for (int i = 0; i<nightList.size(); i++) {
                double temp = Place.getDistance(resultList.get(i), p);
                if (distance > temp) {
                    distance = temp;
                    target = nightList;
                }
            }
            target.add(p);
        }
        morningList = sortByDistance(morningList);

        Place startPoint = morningList.get(morningList.size()-1);
        afternoonList.add(startPoint);
        afternoonList = sortByDistance(startPoint, afternoonList);
        afternoonList.remove(startPoint);

        startPoint = afternoonList.get(afternoonList.size()-1);
        nightList.add(startPoint);
        nightList = sortByDistance(startPoint, nightList);
        nightList.remove(startPoint);
        daySchedule.morningList = morningList;
        daySchedule.afternoonList = afternoonList;
        daySchedule.nightList = nightList;
        return daySchedule;
    }

    private static Place nextPlace(ArrayList<Place> places, Place place){
        for(int i = 0 ; i < places.size(); i++){
            if(places.get(i).equals(place)){
                try {
                    Place p = places.get(i + 1);
                    return p;
                } catch( Exception e){
                    return null;
                }
            }
        }
        return null;
    }

    public static ArrayList<Place> sortByDistance(Place startPosition, ArrayList<Place> placeLine){
        Place[] tempPlaces = (Place[]) placeLine.toArray(new Place[placeLine.size()]);
        Place temp;

        Place startPoint = startPosition;

        for(int i = 0; i < tempPlaces.length; i++){
            if(tempPlaces[i].equals(startPoint)){
                temp = tempPlaces[0];
                tempPlaces[0] = startPoint;
                tempPlaces[i] = temp;
                break;
            }
        }

        double betweenDistance;
        int targetIndex = -1;

        for(int i = 0; i < tempPlaces.length; i++){
            startPoint = tempPlaces[i];
            betweenDistance = Double.MAX_VALUE;
            targetIndex = -1;
            for(int j = i + 1; j < tempPlaces.length; j++){
                double dis = Place.getDistance(startPoint, tempPlaces[j]);
                if(betweenDistance > dis){
                    betweenDistance = dis;
                    targetIndex = j;
                }
            }

            if(targetIndex > -1){
                temp = tempPlaces[i+1];
                tempPlaces[i+1] = tempPlaces[targetIndex];
                tempPlaces[targetIndex] = temp;
            }
        }

        Log.i(TAG, "sortByDistance: finish");
        ArrayList<Place> sortByDistanceList = new ArrayList<Place>(Arrays.asList(tempPlaces));
        return sortByDistanceList;
    }

    public ArrayList<Place> sortByDistance(ArrayList<Place> allPlaceList){
        Place farPoint1= null;
        Place farPoint2 = null;
        double distance = 0;
        for(Place place : allPlaceList){
            for(int i = 1; i < allPlaceList.size(); i++){
                double temp = Place.getDistance(place, allPlaceList.get(i));
                if(distance < temp){
                    distance = temp;
                    farPoint1 = place;
                    farPoint2 = allPlaceList.get(i);
                }
            }
        }
        Place[] tempPlaces = (Place[]) allPlaceList.toArray(new Place[allPlaceList.size()]);
        Place temp;

        Place startPoint = farPoint1;

        for(int i = 0; i < tempPlaces.length; i++){
            if(tempPlaces[i].equals(startPoint)){
                temp = tempPlaces[0];
                tempPlaces[0] = startPoint;
                tempPlaces[i] = temp;
                break;
            }
        }

        double betweenDistance;
        int targetIndex = -1;

        for(int i = 0; i < tempPlaces.length; i++){
            startPoint = tempPlaces[i];
            betweenDistance = Double.MAX_VALUE;
            targetIndex = -1;
            for(int j = i + 1; j < tempPlaces.length; j++){
                double dis = Place.getDistance(startPoint, tempPlaces[j]);
                if(betweenDistance > dis){
                    betweenDistance = dis;
                    targetIndex = j;
                }
            }

            if(targetIndex > -1){
                temp = tempPlaces[i+1];
                tempPlaces[i+1] = tempPlaces[targetIndex];
                tempPlaces[targetIndex] = temp;
            }
        }

        Log.i(TAG, "sortByDistance: finish");
        sortByDistanceList = new ArrayList<Place>(Arrays.asList(tempPlaces));
        return sortByDistanceList;
    }

    public ArrayList<Place> sortByDistance(){
        return sortByDistance(allPlaceList);
    }

    public Schedule getScheduleByDistance(){
        return listToSchedule(sortByDistance());
    }

    public Schedule getSortDistanceScheduleByDay(){
        Schedule schedule = new Schedule(mSchedule.getName(),mSchedule.getStartDate(),mSchedule.getDay_period());

        for(int i = 1 ;i < mSchedule.getDay_period(); i++){
            Schedule.DaySchedule daySchedule = mSchedule.getSchedule(i);
            ArrayList<Place> sorted = sortByDistance(daySchedule.getPlaceList());
            ListIterator<Place> it = sorted.listIterator();

            int morningMinute = Schedule.MORNING_MINUTE;
            int afternoonMinute = Schedule.AFTERNOON_MINUTE;
            int nightMinute = Schedule.NIGHT_MINUTE;

            while(it.hasNext()){
                Place place = it.next();
                int useMinute = place.recommendMinute;
                if(it.hasNext()){
                    double dis = Place.getDistance(place, it.next());
                    useMinute += (int) (dis / walk_speed);
                    it.previous();
                }
                if(morningMinute > 0){
                    morningMinute -= useMinute;
                    schedule.add(i, Schedule.TIME_MORNING, place);
                }else if (afternoonMinute > 0){
                    afternoonMinute = afternoonMinute - useMinute + morningMinute;
                    morningMinute = 0;
                    schedule.add(i, Schedule.TIME_AFTERNOON, place);
                }else if( nightMinute > 0){
                    nightMinute = nightMinute - useMinute + afternoonMinute;
                    afternoonMinute = 0;
                    schedule.add(i, Schedule.TIME_NIGHT, place);
                }else {
                    nightMinute = 0;
                    schedule.add(i, Schedule.TIME_NIGHT, place);
                }
            }
        }
        return schedule;
    }

    public Schedule dayPlaceToSchedule(Map<Integer, ArrayList<Place>> dayMap){
        Schedule schedule = new Schedule(mSchedule.getName(),mSchedule.getStartDate(),mSchedule.getDay_period());

        int morningMinute = Schedule.MORNING_MINUTE;
        int afternoonMinute = Schedule.AFTERNOON_MINUTE;
        int nightMinute = Schedule.NIGHT_MINUTE;

        for(Integer i: dayMap.keySet()){
            ArrayList<Place> places = dayMap.get(i);
            ListIterator<Place> it = places.listIterator();
            while(it.hasNext()){
                Place place = it.next();
                int useMinute = place.recommendMinute;
                if(it.hasNext()){
                    double dis = Place.getDistance(place, it.next());
                    useMinute += (int) (dis / walk_speed);
                    it.previous();
                }
                if(morningMinute > 0){
                    morningMinute -= useMinute;
                    schedule.add(i, Schedule.TIME_MORNING, place);
                }else if (afternoonMinute > 0){
                    afternoonMinute = afternoonMinute - useMinute + morningMinute;
                    morningMinute = 0;
                    schedule.add(i, Schedule.TIME_AFTERNOON, place);
                }else if( nightMinute > 0){
                    nightMinute = nightMinute - useMinute + afternoonMinute;
                    afternoonMinute = 0;
                    schedule.add(i, Schedule.TIME_NIGHT, place);
                }else {
                    nightMinute = 0;
                    schedule.add(i, Schedule.TIME_NIGHT, place);
                }
            }
        }
        return schedule;
    }

    public Schedule listToSchedule(ArrayList<Place> list){
        Schedule schedule = new Schedule(mSchedule.getName(),mSchedule.getStartDate(),mSchedule.getDay_period());
        int morningMinute = Schedule.MORNING_MINUTE;
        int afternoonMinute = Schedule.AFTERNOON_MINUTE;
        int nightMinute = Schedule.NIGHT_MINUTE;
        int totalMinute = Schedule.TOTAL_MINUTE_IN_DAY * schedule.getDay_period();
        double walk_speed = 0.8;
        int currentDay = 1;
        ListIterator<Place> it = list.listIterator();
        while(it.hasNext()){
            Place place = it.next();
            int useMinute = place.recommendMinute;
            if(it.hasNext()){
                double dis = Place.getDistance(place, it.next());
                useMinute += (int) (dis / walk_speed);
                it.previous();
            }
            if(morningMinute > 0){
                morningMinute -= useMinute;
                schedule.add(currentDay, Schedule.TIME_MORNING, place);
            }else if (afternoonMinute > 0){
                afternoonMinute = afternoonMinute - useMinute + morningMinute;
                morningMinute = 0;
                schedule.add(currentDay, Schedule.TIME_AFTERNOON, place);
            }else if( nightMinute > 0){
                nightMinute = nightMinute - useMinute + afternoonMinute;
                afternoonMinute = 0;
                schedule.add(currentDay, Schedule.TIME_NIGHT, place);
            }else if (currentDay + 1 <= schedule.getDay_period()){
                currentDay += 1;
                morningMinute = Schedule.MORNING_MINUTE - useMinute;
                afternoonMinute = Schedule.AFTERNOON_MINUTE ;
                nightMinute = Schedule.NIGHT_MINUTE ;
                schedule.add(currentDay, Schedule.TIME_MORNING, place);
            }else{
                schedule.add(currentDay, Schedule.TIME_NIGHT, place);
            }
        }

        return schedule;
    }

    public void printSchedule(){
        for(int i = 1; i <= mSchedule.getDay_period(); i++){
            Schedule.DaySchedule daySchedule = mSchedule.getSchedule(i);
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            long curr_day_ms = daySchedule.getDate();
            String curr_day = formatter.format(new Date(curr_day_ms));
            Log.i(TAG, "curr day: " + curr_day);
            for(int j = 0 ; j <= 2; j++){
                ArrayList<Place> time_schedule = daySchedule.getTimeSchedule(j);
                if(j == 0){
                    Log.i(TAG, "am");
                }else if(j == 1){
                    Log.i(TAG, "pm");
                }else if(j == 2){
                    Log.i(TAG, "night");
                }
                for(Place place : time_schedule){
                    Log.i(TAG, place.name + ", " + place.openHour + ", " + place.recommendMinute);
                }
            }
        }
    }

    public void printAllPlace(){
        for(Place place : allPlaceList){
            Log.i(TAG, place.name);
        }
    }

}
