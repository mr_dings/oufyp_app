package com.dingsgo.oufyp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatDrawableManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ResultActivity extends AppCompatActivity {

    public static final int KEYWORD_SEARCH = 1;
    public static final int CATEGORY_TOURIST = 11;
    public static final int CATEGORY_EVENT = 12;
    public static final int MODE_NEARBY = 100;
    public static final int MODE_GET = 1000;
    private ListView lv_result;
    private String mKeyword;
    private String mQuery;
    private Toolbar toolbar;
    private int mCategory = CATEGORY_TOURIST;

    private ImageManager imageManger;
    private ArrayList result_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tourist_result);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        ActionBar bar = getSupportActionBar();
        setSupportActionBar(toolbar);
        toolbar.setOnMenuItemClickListener(onMenuItemClick);
        toolbar.setTitle("Result");
//        toolbar.setNavigationIcon(android.R.drawable.ic_menu_revert);
        toolbar.setNavigationIcon(AppCompatDrawableManager.get().getDrawable(this, R.drawable.ic_arrow_back_white_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                ResultActivity.this.setResult(RESULT_CANCELED);
                ResultActivity.this.finish();
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                lv_result.setSelection(0);
            }
        });
        handleRequest();
    }

    private void showResult(final ArrayList<Place> result_list) {
        imageManger = new ImageManager(this);

        lv_result = (ListView) findViewById(R.id.lv_result);

        lv_result.setAdapter(new ResultAdapter(result_list));
        lv_result.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Place selectedItem = result_list.get(position);

                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), DetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("name", selectedItem.name);
                bundle.putInt("category", mCategory);
                intent.putExtra("data", bundle);
                startActivity(intent);

            }
        });
    }

    private void handleRequest() {
        Intent intent = getIntent();

        Bundle bundleExtra = intent.getBundleExtra("data");
        int mode = bundleExtra.getInt("mode", -1);
        mKeyword = bundleExtra.getString("keyword");
        mCategory = bundleExtra.getInt("category");
        mQuery = bundleExtra.getString("query", "");
        if(mode == MODE_NEARBY){
            result_list = (ArrayList) bundleExtra.getSerializable("list");
            showResult(result_list);
            return;
        }
        toolbar.setSubtitle(mKeyword);

        if(mCategory == CATEGORY_TOURIST)
            touristAction();
        else if (mCategory == CATEGORY_EVENT)
            eventAction();
        else {
            throw new RuntimeException("the pass category does not exist");
        }
    }

    private void touristAction(){
        TouristManager tm = new TouristManager(this);
        if(mQuery.equals("")) {
            if (mKeyword.isEmpty()) {
                result_list = tm.getAll();
            } else {
                result_list = tm.query(mKeyword);
            }
        }else{
            result_list = tm.getCategory(mQuery);
        }
        showResult(result_list);
    }

    private void eventAction(){
        EventManager em = new EventManager(this);

        if(mKeyword.isEmpty()){
            result_list = em.getAll();
        }else{
            result_list = em.query(mKeyword);
        }
        showResult(result_list);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_map, menu);
        return true;
    }

    private Toolbar.OnMenuItemClickListener onMenuItemClick = new Toolbar.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            String msg = "";
            switch (menuItem.getItemId()) {
                case R.id.action_map:
                    Intent intent = new Intent();
                    intent.setClass(getApplicationContext(), MapActivity.class);
                    intent.putExtra("places", result_list);
                    startActivity(intent);
                    break;
            }

            return true;
        }
    };

    private class ResultAdapter extends BaseAdapter{

        private static final String TAG = "ResultAdapter";
        private List<Place> list;
//        private String[] urls = {"http://imgsrc.baidu.com/anxun/pic/item/9213b07eca80653837c4bb7794dda144ad3482a3.jpg",
//        "http://img.9ku.com/geshoutuji/singertuji/5/58050/58050_2.jpg",
//        "http://thumbs.dreamstime.com/z/none-51004642.jpg",
//        "http://pic.people.com.cn/NMediaFile/2014/1114/MAIN201411140836000572352132941.jpg"};

        ResultAdapter(List<Place> list){
            this.list = list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            Place place = list.get(position);
            Log.i(TAG, "getView: position = " + position + " || " + place.toString());
            ViewHolder holder;
            if (convertView == null) {
//                Log.i(TAG, "getView: convert view is null");
                convertView = View.inflate(getApplicationContext(),R.layout.listview_drop_down_box,null);
                holder = new ViewHolder();
                holder.iv_small_icon = (ImageView) convertView.findViewById(R.id.iv_small_icon);
                holder.tv_title = (TextView) convertView.findViewById(R.id.tv_title);
                holder.tv_description = (TextView) convertView.findViewById(R.id.tv_description);
                convertView.setTag(holder);
            } else {
//                Log.i(TAG, "getView: not null");
                holder = (ViewHolder) convertView.getTag();
            }
            holder.iv_small_icon.setTag(place.imgSrc);
            Log.i(TAG, "getView: imgSrc -> " + place.imgSrc);
//            Log.i(TAG, "getView: " + holder.iv_small_icon.getTag());
            if(holder.iv_small_icon.getTag().equals(place.imgSrc))
                imageManger.display(holder.iv_small_icon, place.imgSrc);
            holder.tv_title.setText(list.get(position).name);
            holder.tv_description.setText(list.get(position).description);
            return convertView;
        }

        class ViewHolder {
            ImageView iv_small_icon;
            TextView tv_title;
            TextView tv_description;
        }
    }

}
