package com.dingsgo.oufyp.scheduleProcess;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.dingsgo.oufyp.DetailActivity;
import com.dingsgo.oufyp.Event;
import com.dingsgo.oufyp.EventManager;
import com.dingsgo.oufyp.ImageManager;
import com.dingsgo.oufyp.PickerView;
import com.dingsgo.oufyp.Place;
import com.dingsgo.oufyp.R;
import com.dingsgo.oufyp.Schedule;
import com.dingsgo.oufyp.Tourist;
import com.dingsgo.oufyp.TouristManager;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SearchTouristActivity extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener{

    private static final String TAG = "SearchTouristActivity";
    private DrawerLayout mDrawerLayout;
    private TouristManager touristManager;
    private EventManager eventManager;

    private SchedulePreviewAdapter previewAdapter;
    private MyAdapter mAdapter;
    private ListView result_list_view, sListView;
    private EditText et_keyword;
    private CheckBox cb_1, cb_2, cb_3, cb_4, cb_5;
    private Button btn_search;

    private ArrayList<Place> allList;
    private ArrayList<Place> resultList;

    private boolean filter_high_light, filter_event, filter_culture, filter_art_entertainment, filter_out_door;
    private boolean isDateReady = false, change = false;
    private Schedule mSchedule;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_search_tourist);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        // enable ActionBar app icon to behave as action to toggle nav drawer
        ActionBar ab = getSupportActionBar();
        ab.setLogo(R.drawable.ic_filter);
        ab.setHomeButtonEnabled(true);
        ab.setDisplayUseLogoEnabled(true);
        ab.setDisplayHomeAsUpEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon

        ActionBarDrawerToggle abt = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_filter,0,0);

        setTitle("Attraction");
        abt.syncState();
        mDrawerLayout.setDrawerListener(abt);
        init();
    }

    private void init(){

        Intent intent = getIntent();
        mSchedule = (Schedule) intent.getSerializableExtra("schedule");
        touristManager = new TouristManager(this);
        eventManager = new EventManager(this);

        et_keyword = (EditText) findViewById(R.id.et_keyword);
        cb_1 = (CheckBox) findViewById(R.id.cb_1);
        cb_2 = (CheckBox) findViewById(R.id.cb_2);
        cb_3 = (CheckBox) findViewById(R.id.cb_3);
        cb_4 = (CheckBox) findViewById(R.id.cb_4);
        cb_5 = (CheckBox) findViewById(R.id.cb_5);
        btn_search = (Button) findViewById(R.id.btn_search);
        btn_search.setOnClickListener(this);
        result_list_view = (ListView) findViewById(R.id.lv_result);
        sListView = (ListView) findViewById(R.id.sListView);
        mAdapter = new MyAdapter();

        filter_high_light = filter_event = filter_culture = filter_art_entertainment = filter_out_door = true;

        cb_1.setOnCheckedChangeListener(this);
        cb_2.setOnCheckedChangeListener(this);
        cb_3.setOnCheckedChangeListener(this);
        cb_4.setOnCheckedChangeListener(this);
        cb_5.setOnCheckedChangeListener(this);

        allList = new ArrayList<>();

        allList.addAll(touristManager.getTouirstWithLocation());
        allList.addAll(eventManager.getEventsByDayTime(mSchedule.getStartDate(), mSchedule.getEndDate()));
        resultList = allList;
        result_list_view.setAdapter(mAdapter);
        if(mSchedule != null) {
//                    Log.i(TAG, "run: mSchedule do have somethings" );
            previewAdapter = new SchedulePreviewAdapter(getApplicationContext(), mSchedule);
            sListView.setAdapter(previewAdapter);
        }
        isDateReady = true;

        result_list_view.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public static final String TAG = "OnItemLongClickListener";

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Place selectedItem = resultList.get(position);

                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), DetailActivity.class);

                Bundle bundle = new Bundle();
                bundle.putString("name", selectedItem.name);
                intent.putExtra("data", bundle);
                startActivity(intent);
                return true;
            }
        });

        result_list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showSimpleDialog(position);
            }
        });
    }

    private void showSimpleDialog(final int position) {
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        View view = View.inflate(this, R.layout.dialog_add_schedule, null);

        final PickerView day_pv = (PickerView) view.findViewById(R.id.day_pv);
        final PickerView time_pv = (PickerView) view.findViewById(R.id.time_pv);

        List<String> day = new ArrayList<String>();
        List<String> time = new ArrayList<String>();
        for(int i = 1; i <= mSchedule.getDay_period(); i++){
            day.add(i+"");
        }
        time.add("morning");
        time.add("afternoon");
        time.add("night");

        day_pv.setData(day);
        time_pv.setData(time);

        builder.setView(view);
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public static final String TAG = "dialogClick";

            @Override
            public void onClick(DialogInterface dialog, int which) {
                int day = Integer.parseInt(day_pv.getSelected());
                int time = 0;
                if(time_pv.getSelected().equals("morning")){
                    time = Schedule.TIME_MORNING;
                }else if(time_pv.getSelected().equals("afternoon")){
                    time = Schedule.TIME_AFTERNOON;
                }else if(time_pv.getSelected().equals("night")){
                    time = Schedule.TIME_NIGHT;
                }
                mSchedule.add(day, time, resultList.get(position));
                previewAdapter.setDate(mSchedule);
                previewAdapter.notifyDataSetChanged();
                change = true;
            }
        });
        builder.setNegativeButton("取消", null);
        //设置对话框是可取消的
        builder.setCancelable(true);
        builder.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.schedule_search_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
//                if(mDrawerLayout.isDrawerOpen(GravityCompat.START))
//                    mDrawerLayout.closeDrawer(GravityCompat.START);
//                else
//                    mDrawerLayout.openDrawer(GravityCompat.START);
                break;
            case R.id.action_filter:
                if(mDrawerLayout.isDrawerOpen(GravityCompat.START))
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                else
                    mDrawerLayout.openDrawer(GravityCompat.START);
                break;
            case R.id.action_information:
                if(mDrawerLayout.isDrawerOpen(GravityCompat.END))
                    mDrawerLayout.closeDrawer(GravityCompat.END);
                else
                mDrawerLayout.openDrawer(GravityCompat.END);
                break;
            case R.id.action_ok:
                Intent intent = new Intent();
                intent.putExtra("schedule", mSchedule);
                setResult(RESULT_OK, intent);
                this.finish();
                break;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        if(change){
            showSimpleDialog();
        }else{
            this.finish();
        }
    }

    private void showSimpleDialog() {
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setTitle("warning");
        builder.setMessage("do you want to add the select data?");

        //监听下方button点击事件
        builder.setPositiveButton("leave", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                setResult(RESULT_CANCELED);
                SearchTouristActivity.this.finish();
            }
        });
        builder.setNegativeButton("cacel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.setNeutralButton("add and leave", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent();
                intent.putExtra("schedule", mSchedule);
                setResult(RESULT_OK, intent);
                SearchTouristActivity.this.finish();
            }
        });

        //设置对话框是可取消的
        builder.setCancelable(true);
        AlertDialog dialog=builder.create();
        dialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_search:
                search();
                break;
        }
    }

    private void search() {

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()){
            case R.id.cb_1:
                filter_high_light = isChecked;
                break;
            case R.id.cb_2:
                filter_event = isChecked;
                break;
            case R.id.cb_3:
                filter_culture = isChecked;
                break;
            case R.id.cb_4:
                filter_art_entertainment = isChecked;
                break;
            case R.id.cb_5:
                filter_out_door = isChecked;
                break;
        }
//        Log.i(TAG, "onCheckedChanged: " + filter_high_light + " || " + filter_event);
        mAdapter.filtCategory();
    }

    class MyAdapter extends BaseAdapter{

        private static final String TAG = "MyAdapter";
        ImageManager imageManger = new ImageManager(getApplicationContext());

        @Override
        public int getCount() {
            return resultList.size();
        }

        public void filtCategory(){
            ArrayList<Place> list = new ArrayList<>();
            for(Place place : allList){
                if(place instanceof Tourist){
                    String tag1 = ((Tourist) place).tag1;
                    String tag2 = ((Tourist) place).tag2;
                    String tag3 = ((Tourist) place).tag3;
                    if(filter_high_light){
                        if(tag1.equals(Tourist.CATEGORY_HIGHT_LIGHT) || tag2.equals(Tourist.CATEGORY_HIGHT_LIGHT) || tag3.equals(Tourist.CATEGORY_HIGHT_LIGHT)){
                            list.add(place);
                            continue;
                        }
                    }
                    if(filter_culture){
                        if(tag1.equals(Tourist.CATEGORY_CULTURE) || tag2.equals(Tourist.CATEGORY_CULTURE) || tag3.equals(Tourist.CATEGORY_CULTURE)){
                            list.add(place);
                            continue;
                        }
                    }
                    if(filter_art_entertainment){
                        if(tag1.equals(Tourist.CATEGORY_ART_ENTERTAINMENT) || tag2.equals(Tourist.CATEGORY_ART_ENTERTAINMENT) || tag3.equals(Tourist.CATEGORY_ART_ENTERTAINMENT)){
                            list.add(place);
                            continue;
                        }
                    }
                    if(filter_out_door){
                        if(tag1.equals(Tourist.CATEGORY_OUTDOOR) || tag2.equals(Tourist.CATEGORY_OUTDOOR) || tag3.equals(Tourist.CATEGORY_OUTDOOR)){
                            list.add(place);
                            continue;
                        }
                    }
                }else if(place instanceof Event){
                    if(filter_event){
                        list.add(place);
                    }
                }
            }
            resultList = list;
            notifyDataSetChanged();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Place place = resultList.get(position);
//            Log.i(TAG, "getView: position = " + position + " || " + place.toString());
            ViewHolder holder;
            if (convertView == null) {
//                Log.i(TAG, "getView: convert view is null");
                convertView = View.inflate(getApplicationContext(),R.layout.listview_drop_down_box,null);
                holder = new ViewHolder();
                holder.iv_small_icon = (ImageView) convertView.findViewById(R.id.iv_small_icon);
                holder.tv_title = (TextView) convertView.findViewById(R.id.tv_title);
                holder.tv_description = (TextView) convertView.findViewById(R.id.tv_description);
                convertView.setTag(holder);
            } else {
//                Log.i(TAG, "getView: not null");
                holder = (ViewHolder) convertView.getTag();
            }
            holder.iv_small_icon.setTag(place.imgSrc);
//            Log.i(TAG, "getView: imgSrc -> " + place.imgSrc);
//            Log.i(TAG, "getView: " + holder.iv_small_icon.getTag());
            if(holder.iv_small_icon.getTag().equals(place.imgSrc))
                imageManger.display(holder.iv_small_icon, place.imgSrc);
            holder.tv_title.setText(resultList.get(position).name);
            int openHour = place.openHour < 0 ? 0 : place.openHour;
            int endHour = place.endHour < 0 ? 2400 : place.endHour;
            holder.tv_description.setText(openHour + " - " + endHour);
            return convertView;
        }

        class ViewHolder {
            ImageView iv_small_icon;
            TextView tv_title;
            TextView tv_description;
        }
    }

    class SchedulePreviewAdapter extends BaseAdapter{

        private Schedule current_scheule;
        private ArrayList list;
        private Context context;

        SchedulePreviewAdapter(Context context, Schedule schedule){
            this.context = context;
            setDate(schedule);
        }

        public void setDate(Schedule schedule){
            this.current_scheule = schedule;
            list = new ArrayList<>();
            int index = 0;
            for(int i = 1; i <= schedule.getDay_period(); i++){
                Schedule.DaySchedule daySchedule = schedule.getSchedule(i);
                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                long curr_day_ms = daySchedule.getDate();
                String curr_day = formatter.format(new Date(curr_day_ms));
                for(int j = 0 ; j <= 2; j++){
                    ArrayList<Place> time_schedule = daySchedule.getTimeSchedule(j);
                    if(j == 0){
                        list.add(curr_day + "\n am");
                    }else if(j == 1){
                        list.add("pm");
                    }else if(j == 2){
                        list.add("night");
                    }
                    for(Place place : time_schedule){
                        list.add(place);
                    }
                }
            }
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = null;
            Object obj = list.get(position);
            if(obj instanceof Place){
                Place place = (Place) obj;
                v = View.inflate(context, R.layout.listview_schedule_drop_down_preview_item, null);
                TextView tv_name = (TextView) v.findViewById(R.id.tv_name);
                TextView tv_recommendMinute = (TextView) v.findViewById(R.id.tv_recommendMinute);
                tv_name.setText(place.name);
                tv_recommendMinute.setText("expect : " + place.recommendMinute + "min");
            }else if ( obj instanceof String){
                v = View.inflate(context, R.layout.schedule_list_category_item, null);
                TextView tv = (TextView) v.findViewById(R.id.tv_list_category);
                tv.setText(obj.toString());
            }
            return v;
        }
    }
}
