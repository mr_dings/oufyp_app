package com.dingsgo.oufyp.scheduleProcess;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.dingsgo.oufyp.DetailActivity;
import com.dingsgo.oufyp.Event;
import com.dingsgo.oufyp.ImageManager;
import com.dingsgo.oufyp.Place;
import com.dingsgo.oufyp.R;
import com.dingsgo.oufyp.Schedule;
import com.dingsgo.oufyp.ScheduleUtil;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import static android.content.ContentValues.TAG;

public class ScheduleEditActivity extends AppCompatActivity {

    private static final String TAG = "ScheduleEditActivity";
    private static final int REQUEST_MAP = 222;
    private static final int REQUEST_SEARCH = 111;

    private ScheduleListView slv;
    private ScheduleAdapter scheduleAdapter = null;
    private Schedule mSchedule;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_schedule);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        mSchedule = (Schedule) intent.getSerializableExtra("schedule");
        Log.i(TAG, "onCreate: " + mSchedule.toString());
        setTitle(mSchedule.getName());

//        Log.i(TAG, "onCreate: ---------------- " + schedule.toString());
        slv = (ScheduleListView) findViewById(R.id.sListView);
        scheduleAdapter = new ScheduleAdapter(this, mSchedule);
        slv.setAdapter(scheduleAdapter);
        slv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), DetailActivity.class);
                Bundle bundle = new Bundle();
                Object obj = scheduleAdapter.getItem(position);
                String name = "";
                if(obj instanceof String)
                    return;
                else
                    name = ((Place)obj).getName();
                bundle.putString("name", name);
                intent.putExtra("data", bundle);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onBackPressed() {
        if(scheduleAdapter.getChange()){
            showSimpleDialog();
        }else{
            this.finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        new MenuInflater(this).inflate(R.menu.menu_schedule_list, menu);
        for(int j = 0; j < menu.size(); j++){
            MenuItem item = menu.getItem(j);
            item.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS);
            if(item.getItemId() == R.id.action_save){
                if(!scheduleAdapter.isEditable()){
                    item.setVisible(false);
                }
            }
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_add:
                if(!scheduleAdapter.isEditable()) {
                    scheduleAdapter.setEditable(true);
                    invalidateOptionsMenu();
                    return super.onOptionsItemSelected(item);
                }
                Intent addIntent = new Intent();
                addIntent.setClass(this, SearchTouristActivity.class);
                addIntent.putExtra("schedule", scheduleAdapter.getResult());
                startActivityForResult(addIntent, REQUEST_SEARCH);
                return true;
            case R.id.action_map:
                Intent intent = new Intent();
                intent.setClass(this, ScheduleMapActivity.class);
                intent.putExtra("schedule", scheduleAdapter.getResult());
                startActivityForResult(intent, REQUEST_MAP);
                return true;
            case R.id.action_save:
                scheduleAdapter.setEditable(false);
                try {
                    Schedule.saveAsLocalStorage(getApplicationContext(),scheduleAdapter.getResult());
                    scheduleAdapter.setChange(false);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                invalidateOptionsMenu();
                return true;
            case R.id.action_sort_distance:
                ScheduleUtil util = new ScheduleUtil(scheduleAdapter.getResult());
                Schedule schedule = util.getSortDistanceScheduleByDay();
                scheduleAdapter.setDate(schedule);
                scheduleAdapter.notifyDataSetChanged();
                scheduleAdapter.setChange(true);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showSimpleDialog() {
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle("warning");
        builder.setMessage("Haven't save the schedule, do you want to exit this page?");

        //监听下方button点击事件
        builder.setPositiveButton("leave", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                ScheduleEditActivity.this.finish();
            }
        });
        builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.setNeutralButton("save and leave", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                try {
                    Schedule.saveAsLocalStorage(getApplicationContext(),scheduleAdapter.getResult());
                    ScheduleEditActivity.this.finish();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        //设置对话框是可取消的
        builder.setCancelable(true);
        AlertDialog dialog=builder.create();
        dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK && requestCode == REQUEST_SEARCH){
            Schedule sc = (Schedule) data.getSerializableExtra("schedule");
            scheduleAdapter.setDate(sc);
            scheduleAdapter.notifyDataSetChanged();
            scheduleAdapter.setChange(true);
        }

        if(resultCode == RESULT_OK && requestCode == REQUEST_MAP){
            Schedule sc = (Schedule) data.getSerializableExtra("schedule");
            scheduleAdapter.setDate(sc);
            scheduleAdapter.notifyDataSetChanged();
            scheduleAdapter.setChange(true);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}

class ScheduleAdapter extends BaseAdapter implements View.OnClickListener{

    public static final int TYPE_CATEGORY_ITEM = 0;
    public static final int TYPE_ITEM = 1;
    private Schedule current_scheule;
    private ArrayList list;
    private Context context;
    private boolean change = false;
    private int count = 0;
    DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    ImageManager imageManger;
    private boolean editable = false;

    ScheduleAdapter(Context context, Schedule schedule){
        this.context = context;
        setDate(schedule);
        imageManger = new ImageManager(context);
        editable = false;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
        notifyDataSetInvalidated();
    }

    public void setDate(Schedule schedule){
        this.current_scheule = schedule;
        list = new ArrayList<>();
        int index = 0;
        for(int i = 1; i <= schedule.getDay_period(); i++){
            Schedule.DaySchedule daySchedule = schedule.getSchedule(i);
            long curr_day_ms = daySchedule.getDate();
            String curr_day = formatter.format(new Date(curr_day_ms)) + " (Day " + i +")";
            for(int j = 0 ; j <= 2; j++){
                ArrayList<Place> time_schedule = daySchedule.getTimeSchedule(j);
                if(j == 0){
                    list.add(curr_day + "\n Morning");
                }else if(j == 1){
                    list.add(" Afternoon");
                }else if(j == 2){
                    list.add(" Night");
                }
                for(Place place : time_schedule){
                    list.add(place);
                }
            }
        }
    }

    public boolean getChange(){
        return this.change;
    }

    public void setChange(boolean change){
        this.change = change;
    }

    public Schedule getResult(){
        Schedule schedule = new Schedule(this.current_scheule.getName(), this.current_scheule.getStartDate(), this.current_scheule.getDay_period());
        int time = -1;
        int day = 1;
        Schedule.DaySchedule ds;
        for(Object obj: list){
            if(obj instanceof String){
                time++;
                if(time == 3){
                    time = 0;
                    day++;
                    continue;
                }
            }else{
                Place place = (Place) obj;
                schedule.add(day, time, place);
            }
        }
        return schedule;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public int getViewTypeCount() {
        return super.getViewTypeCount();
    }

    @Override
    public int getItemViewType(int position) {
        if(position == -1){
            return TYPE_CATEGORY_ITEM;
        }
        Object obj = list.get(position);
        if(obj instanceof Place){
            return TYPE_ITEM;
        }else
            return TYPE_CATEGORY_ITEM;

    }

    @Override
    public void onClick(View v) {
        ImageButton btn = (ImageButton) v;
        String description = (String) v.getTag();

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(description);
        builder.setTitle("Warning");
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
            }
        });
        builder.create().show();

    }

    class TimeCategory {
        int time;
        int day;
    }

    private TimeCategory getTimeCategory(int position){
        int time = -1;
        int day = 1;
        TimeCategory t = new TimeCategory();
        for(int i = 0; i < position; i++){
            Object obj = list.get(i);
            if(obj instanceof String){
                time ++;
                if(time == 3) {
                    time = 0;
                    day++;
                }
            }
        }
        t.time = time;
        t.day = day;
        return t;
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public void swapData(int from, int to){
        // mDragDatas是List实例对象
        Collections.swap(list, from, to);
        notifyDataSetChanged();
        change = true;
    }

    public void remove(int position){
        list.remove(position);
        change = true;
        notifyDataSetChanged();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = null;
        Object obj = list.get(position);
        if(obj instanceof Place){
            v = View.inflate(context, R.layout.schedule_list_item, null);
            Place place = ((Place) obj);

            ImageView iv_ic = (ImageView) v.findViewById(R.id.iv_ic);

            imageManger.display(iv_ic, place.imgSrc);


            TextView tv_name = (TextView) v.findViewById(R.id.tv_name);
            TextView tv_openHour = (TextView) v.findViewById(R.id.tv_openHour);
            if(place.name.length() > 15 ){
                tv_name.setText(place.name.substring(0, 12) + "...");
            }else
                tv_name.setText(place.name);
            // openHour = -1 for whole day open
            int openHour = place.openHour < 0 ? 0 : place.openHour;
            // endHour = -1 for whole day open
            int endHour = place.endHour < 0 ? 2400 : place.endHour;
            tv_openHour.setText(openHour + "-" + endHour);
            if(editable) {
                ImageButton btn_delete = (ImageButton) v.findViewById(R.id.btn_delete);
                btn_delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        remove(position);
                    }
                });
                ImageView iv_move = (ImageView) v.findViewById(R.id.iv_move);
                iv_move.setVisibility(View.VISIBLE);
                btn_delete.setVisibility(View.VISIBLE);
            }
            TimeCategory t= getTimeCategory(position);
            int time = t.time;
            int day = t.day;
            Log.i("edit", "time = " + time + ", open hour : " + place.openHour + "  , end hour :   " + endHour);
            boolean flat = false;
            String error_description = "";

            if(place.startDate >0 && place.endDate > 0) {
                Log.i(TAG, "time i : " + t.time + " tiem d : " + t.day);
                Log.i(TAG, "current_scheulde time : " + current_scheule.getStartDate());
                long offsetday = --day * 24 * 60 * 60 * 1000;
                long currentDate = current_scheule.getStartDate() + offsetday;
                long endDate = currentDate + 24 * 60 * 60 * 1000;
                Log.i(TAG, "start date = " + formatter.format(new Date(currentDate)) + " end Date = " + formatter.format(new Date(endDate)));
                Log.i(TAG, "event date = " + formatter.format(new Date(place.startDate)) + " end Date = " + formatter.format(new Date(place.endDate)));
                Log.i(TAG, "current data : " + currentDate);
                Log.i(TAG, "start data : " + place.startDate + " end data : " + place.endDate);
                if(currentDate >= place.startDate && currentDate <= place.endDate){

                }else{
                    flat = true;
                    error_description += "event period : " + formatter.format(new Date(place.startDate)) + " - " +formatter.format(new Date(place.endDate))+ "\ncurrent date : " + formatter.format(new Date(currentDate));
                }
//                if (currentDate >= place.startDate) {
//                    flat = true;
//                    error_description += "event start date : " + formatter.format(new Date(place.startDate)) + "\ncurrent date : " + formatter.format(new Date(currentDate));
//                }
//                if (currentDate > place.endDate) {
//                    flat = true;
//                    error_description += "\nevent end date : " + formatter.format(new Date(place.startDate)) + "\ncurrent date : " + formatter.format(new Date(currentDate));
//                }
            }
            switch (time){
                case Schedule.TIME_MORNING:
                    if(openHour > 0) {
                        if (openHour >= 1200) {
                            flat = true;
                            error_description += "\nThis section can't place in morning";
                        }
                    }
                    break;
                case Schedule.TIME_AFTERNOON:
                    if(endHour > 0){
                        if(endHour <= 1200){
                            flat = true;
                            error_description += "\nThis section can't place in afternoon";
                        }
                    }
                    if(openHour > 0){
                        if(openHour >= 1800){
                            flat = true;
                            error_description += "\nThis section can't place in afternoon";
                        }
                    }
                    break;
                case Schedule.TIME_NIGHT:
                    if(endHour > 0){
                        if(endHour <= 1800){
                            flat = true;
                            error_description += "\nThis section can't place in night";
                        }
                    }
                    break;
            }
            if(flat) {
                ImageButton btn_warning = (ImageButton) v.findViewById(R.id.btn_warning);
                btn_warning.setTag(error_description);
                btn_warning.setOnClickListener(this);
                btn_warning.setVisibility(View.VISIBLE);
            }
        }else if ( obj instanceof String){
            v = View.inflate(context, R.layout.schedule_list_category_item, null);
            TextView tv = (TextView) v.findViewById(R.id.tv_list_category);
            tv.setText(obj.toString());
        }
//            Log.i(TAG, "getView: position =>  " + position + "  || " + list.get(position));
        return v;
    }
}