package com.dingsgo.oufyp.scheduleProcess.task;

import android.os.AsyncTask;
import android.util.Log;

import com.dingsgo.oufyp.MapPlaceInfo;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dings on 15/4/2017.
 */

public class GetNearbyInfo extends AsyncTask<LatLng, Void, List<MapPlaceInfo>> {

//    public static final String HOST = "http://fyp.dingsgo.com/api/search/";
    public static final String HOST = "http://192.168.1.219/fyp/index.php?/api/search";
    private static final String TAG = "GetNearbyInfo";

    private List<MapPlaceInfo> result;

    @Override
    protected List<MapPlaceInfo> doInBackground(LatLng... params) {
        String getParam = "?lat=" + params[0].latitude + "&lng=" + params[0].longitude;
        String response = getURlResponse(HOST + getParam);
        Log.i(TAG, "doInBackground: " + response);
        ArrayList<MapPlaceInfo> infos = null;
        try {
            JSONObject json = new JSONObject(response);
            JSONArray nl = json.getJSONArray("results");
            infos = new ArrayList<>();
            for(int i = 0; i < nl.length(); i++){
                JSONObject nlJson = nl.getJSONObject(i);
                MapPlaceInfo info = MapPlaceInfo.parseJSON(nlJson);
                infos.add(info);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return infos;
    }

    @Override
    protected void onPostExecute(List<MapPlaceInfo> infos) {
        this.result = infos;
    }

    private String getURlResponse(String s) {
        HttpURLConnection httpURLConnection = null;
        InputStream inputStream = null;
        String resultData = "";

        try {
            URL url = new URL(s);   //URl对象
            Log.i(TAG, "getURlResponse: url = " + url);
            httpURLConnection = (HttpURLConnection) url.openConnection();   //使用URl打开一个链接
            httpURLConnection.setDoInput(true);//允许输入流，即允许下载
            httpURLConnection.setDoOutput(true);//允许输出流，即允许上传
            httpURLConnection.setUseCaches(false); //不使用缓冲
            httpURLConnection.setRequestMethod("GET"); //使用get请求

            int responseCode = httpURLConnection.getResponseCode();
            Log.i(TAG, "getURlResponse: response code = " + responseCode);

            inputStream = httpURLConnection.getInputStream();//获取输入流，此时才真正建立链接
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String inputLine;
            while ((inputLine = bufferedReader.readLine()) != null) {
                resultData = inputLine + resultData + "\n";
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputStream != null)
                    inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            httpURLConnection.disconnect();
        }
        return resultData;
    }

    public List<MapPlaceInfo> getResult() {
        return result;
    }
}

