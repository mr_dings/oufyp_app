package com.dingsgo.oufyp.scheduleProcess;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dingsgo.oufyp.DetailActivity;
import com.dingsgo.oufyp.Place;
import com.dingsgo.oufyp.R;
import com.dingsgo.oufyp.Schedule;
import com.dingsgo.oufyp.ScheduleUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

public class ScheduleMapActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    private Schedule schedule;
    private ScheduleUtil scheduleUtil;
    private Place[] originAllPlace;
    private Place[] editAllPlace;
    private Polyline currLine;

    private View customerView;
    private TextView tv_title;
    private Button btn_pre, btn_next;

    private LatLng selectedPosition;
    private Place draggedPlace;
    private Polyline selectedLine;

    private GoogleMap mMap;
    private static final String TAG = "ScheduleMapActivity";

    private LinearLayout map_bottom;
    private LinearLayout map_top;
    private TextView estimate_distance;
    private TextView estimate_time;


    private Map<Integer, ArrayList<Place>> dayPlaces;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        Intent intent = getIntent();
        schedule = (Schedule) intent.getSerializableExtra("schedule");

        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        scheduleUtil = new ScheduleUtil(schedule);
        ArrayList<Place> list = scheduleUtil.getAllPlaceList();

        for(Place place : list){
            Log.i(TAG, "onCreate: " + place.toString());
        }
        dayPlaces = scheduleUtil.getDayPlace();

//        originAllPlace = list.toArray(new Place[list.size()]);
        editAllPlace = list.toArray(new Place[list.size()]);

        map_bottom = (LinearLayout) findViewById(R.id.map_bottom);
        map_top = (LinearLayout) findViewById(R.id.map_top);

    }

    private void init() {
        customerView = View.inflate(this, R.layout.custome_marker, null);
        tv_title = (TextView) customerView.findViewById(R.id.tv_title);
        btn_pre = (Button) customerView.findViewById(R.id.btn_pre);
        btn_next = (Button) customerView.findViewById(R.id.btn_next);
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we
     * just add a marker near Africa.
     */
    @Override
    public void onMapReady(final GoogleMap map) {
        this.mMap = map;
        init();
        LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();

//        int placeSize = originAllPlace.length;
        int placeSize = editAllPlace.length;
        String timePeriodDescription = "";
        LatLng ln = null;
        for(int i = 0; i < editAllPlace.length; i++){
            Place place = editAllPlace[i];
            MarkerOptions mo = new MarkerOptions();
            LatLng latlng = new LatLng(place.lat, place.lon);
            ln = latlng;
            boundsBuilder.include(latlng);
//            mo.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(this, customerView, i)));
            mo.position(latlng);
            mo.title(place.name);
            if(i == 0){
                mo.icon(BitmapDescriptorFactory.fromResource(R.drawable.flag_start));
            }
            if(i == editAllPlace.length - 1){
                mo.icon(BitmapDescriptorFactory.fromResource(R.drawable.flag_end));
            }
            mo.draggable(true);
            map.addMarker(mo);
        }

        mMap.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
            @Override
            public void onCameraMoveStarted(int i) {
                map_bottom.setVisibility(View.INVISIBLE);
                map_top.setVisibility(View.INVISIBLE);
            }
        });

        mMap.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
            @Override
            public void onCameraMoveStarted(int reason) {
                if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
//                    Toast.makeText(ScheduleTrackingMap.this, "The user gestured on the map.",
//                            Toast.LENGTH_SHORT).show();
                    map_bottom.setVisibility(View.INVISIBLE);
                    map_top.setVisibility(View.INVISIBLE);
                } else if (reason == GoogleMap.OnCameraMoveStartedListener
                        .REASON_API_ANIMATION) {
//                    Toast.makeText(ScheduleTrackingMap.this, "The user tapped something on the map.",
//                            Toast.LENGTH_SHORT).show();
                    map_bottom.setVisibility(View.VISIBLE);
                    map_top.setVisibility(View.VISIBLE);
                } else if (reason == GoogleMap.OnCameraMoveStartedListener
                        .REASON_DEVELOPER_ANIMATION) {
//                    Toast.makeText(ScheduleTrackingMap.this, "The app moved the camera.",
//                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                String name = marker.getTitle();
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putString("name", name);
                intent.putExtra("data", bundle);
                intent.setClass(getApplicationContext(), DetailActivity.class);
                startActivity(intent);
            }
        });
        map.setOnMarkerClickListener(this);

        map.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {
//                marker.setVisible(false);
                String title = marker.getTitle();
                MarkerOptions mo = new MarkerOptions();
                for(int i = 0 ; i < editAllPlace.length; i ++){
                    if(editAllPlace[i].name.equals(title)){
                        selectedPosition = new LatLng(editAllPlace[i].lat, editAllPlace[i].lon);
                        mo.position(selectedPosition);
                        break;
                    }
                }
                mo.title(title);
                mo.draggable(true);
                map.addMarker(mo);
                Log.i(TAG, "onMarkerDragStart: " + marker.getTitle());
            }

            @Override
            public void onMarkerDrag(Marker marker) {
                PolylineOptions po = new PolylineOptions();
                po.add(selectedPosition);

                po.add(marker.getPosition());
                if(selectedLine != null)
                    selectedLine.remove();
                selectedLine = map.addPolyline(po);
//                Log.i(TAG, "onMarkerDrag: " + marker.getTitle());
            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                Log.i(TAG, "onMarkerDragEnd: " + marker.getTitle());

                double max = Double.MAX_VALUE;
                for(int i = 0; i < editAllPlace.length; i++){
                    double dis = Place.getDistance(marker.getPosition().latitude, marker.getPosition().longitude, editAllPlace[i].lat, editAllPlace[i].lon);
                    Log.i(TAG, "onMarkerDragEnd: " + dis + " : " + editAllPlace[i].toString());
                    if(max > dis){
                        max = dis;
                        draggedPlace = editAllPlace[i];
                    }
                }

                Log.i(TAG, "onMarkerDragEnd: " + draggedPlace.toString());
                Log.i(TAG, "onMarkerDragEnd: " + max);

                if(selectedLine != null)
                    selectedLine.remove();
                marker.remove();
                if(max > 700){
                    draggedPlace = null;
                    return;
                }

                ArrayList<Place> selfList = null;
                int self_index = -1;
                ArrayList<Place> targetList = null;
                int target_index = -1;

                for(Integer i : dayPlaces.keySet()){
                    ArrayList<Place> curr_places = dayPlaces.get(i);
                    Log.i(TAG, "day " + i + " -------------------" );
                    for(int j = 0 ; j < curr_places.size(); j++){
                        Place p = curr_places.get(j);
                        Log.i(TAG, "loop : " + p.getName() + " || " + marker.getTitle());
                        if(p.getName().equals(marker.getTitle())){
                            selfList = curr_places;
                            self_index = j;
                        }
                        if(p.equals(draggedPlace)){
                            targetList = curr_places;
                            target_index = j;
                        }
                    }
                }
                Log.i(TAG, "onMarkerDragEnd: " + selfList + " - " + targetList);
                if(selfList == targetList){
                    for(Place p1 : selfList){
                        Log.i(TAG, p1.getName());
                    }
                    Log.i(TAG, "-----------------");
                    Place place = selfList.remove(target_index);
                    selfList.add(self_index+1, place);
                    for(Place p1 : selfList){
                        Log.i(TAG, p1.getName());
                    }
                }else{
                    targetList.remove(target_index);
                    selfList.add(self_index+1, draggedPlace);
                }
                refreshDayPlace(dayPlaces);
            }
        });
//        refreshPlace(editAllPlace);

        map.clear();
        refreshDayPlace(dayPlaces);

        /*
        for(int i = 1; i <= schedule.getDay_period(); i++){
            Schedule.DaySchedule daySchedule = schedule.getSchedule(i);
            for(int j = 0 ; j <= 2; j++){
                ArrayList<Place> time_schedule = daySchedule.getTimeSchedule(j);
                DateFormat formatter = new SimpleDateFormat("MM-dd");
                long curr_day_ms = daySchedule.getDate();
                String curr_day = formatter.format(new Date(curr_day_ms));
                if(j == 0){
                    timePeriodDescription = curr_day + " a.m";
                }else if(j == 1){
                    timePeriodDescription = curr_day + " p.m";
                }else if(j == 2){
                    timePeriodDescription = curr_day + " night";
                }
                for(Place place : time_schedule){
                    placeSize++;
                    if(place.lat == 0 && place.lon == 0){
                        continue;
                    }
                    MarkerOptions mo = new MarkerOptions();
                    ln = new LatLng(place.lat, place.lon);
                    boundsBuilder.include(ln);
                    mo.position(ln);
                    mo.title(place.name);
                    mo.snippet(timePeriodDescription);
                    mMap.addMarker(mo);
                    pl.add(ln);
                }
            }
        }
        pl.width(5);
        pl.color(Color.RED);
        mMap.addPolyline(pl);
//        int cameraWidth = dip2px(this, 160);
//        int cameraheight = dip2px(this, 160);
        */

        if(placeSize > 1) {
            int width = getResources().getDisplayMetrics().widthPixels;
            int height = getResources().getDisplayMetrics().heightPixels;
            map.moveCamera(CameraUpdateFactory.newLatLngBounds(boundsBuilder.build(), width, height, 100));
//            mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(boundsBuilder.build(), 100));
        }else if (placeSize == 1){
            LatLng ll = ln;
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(ll,17f));
        }
    }

    private void refreshDayPlace(Map<Integer, ArrayList<Place>> dayPlaces){
        mMap.clear();
        for(Integer day : dayPlaces.keySet()){
            ArrayList<Place> placeList = dayPlaces.get(day);
            PolylineOptions po = new PolylineOptions();
            for(int i = 0 ; i < placeList.size(); i++){
                Place place = placeList.get(i);
//                Log.i(TAG, "refreshDayPlace: " + place.toString());
                LatLng la = new LatLng(place.lat, place.lon);
                po.add(la);
                MarkerOptions mo = new MarkerOptions();
                mo.position(la);
                if(i == 0){
                    mo.icon(BitmapDescriptorFactory.fromResource(R.drawable.flag_start));
                }
                mo.title(place.name);
                mo.draggable(true);
                HintModel hint = new HintModel();
                hint.day = day;
                hint.place = place;
                mMap.addMarker(mo).setTag(hint);
            }
            mMap.addPolyline(po);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        Schedule schedule = scheduleUtil.dayPlaceToSchedule(dayPlaces);
        intent.putExtra("schedule",schedule);
        setResult(RESULT_OK, intent);
        this.finish();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        HintModel hintModel = (HintModel) marker.getTag();
        ArrayList<Place> currList = dayPlaces.get(hintModel.day);

        Log.i(TAG, "onMarkerClick: current day : " + hintModel.day);

        TextView estimate_distance = (TextView) findViewById(R.id.estimate_distance);
        TextView estimate_time = (TextView) findViewById(R.id.estimate_time);
        TextView tv_date = (TextView) findViewById(R.id.tv_date);
        TextView tv_arr = (TextView) findViewById(R.id.tv_arr);
        TextView tv_leave_time = (TextView) findViewById(R.id.tv_leave_time);

        int arr_time = 900;
        int total_min = 0;
        int expect_travel_time = 0;
        int expect_arrivate_time = 0;
        double expect_travel_distance = 0;
        boolean flat = false;
        for(int i = 0; i < currList.size(); i++){
            Place place = currList.get(i);
            try{
                expect_arrivate_time = total_min;
                int useMinute = currList.get(i).recommendMinute;
                double dis = Place.getDistance(place, currList.get(i + 1));
                if(dis < 500) {
                    expect_travel_distance = dis;
                    useMinute += (int) (dis / 45);
                } else {
                    expect_travel_distance = dis;
                    useMinute += (int) (dis / 500);
                }
                total_min += useMinute;
                Log.i(TAG, "onMarkerClick: " + useMinute);
            }catch (Exception e){
                flat = true;
                break;
            }
            if(place.equals(hintModel.place)){
                break;
            }
        }

        estimate_distance.setText("distance to next : "+expect_travel_distance+"m");


        long currDay = schedule.getStartDate() + hintModel.day * 24 * 60 * 60 * 1000;
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        tv_date.setText(formatter.format(new Date(currDay)));
        if(expect_travel_distance < 500)
            expect_travel_time = (int) (expect_travel_distance / 45);
        else
            expect_travel_time = (int) (expect_travel_distance / 500);
        estimate_time.setText("travel to next estimate :" + (expect_travel_time) + "min");

        int temp = 0;
        if(!flat)
            total_min = total_min - expect_travel_time - hintModel.place.getRecommendMinute();
        while(total_min >= 60){
            total_min -= 60;
            temp += 100;
        }
        temp += total_min;
        arr_time += temp;

        int leave_time = arr_time;

        int ttemp = hintModel.place.getRecommendMinute();

        while(ttemp >= 60){
            ttemp -=60;
            leave_time += 100;
        }
        ttemp = leave_time + ttemp;

        tv_leave_time.setText("expect leave time : " + ttemp + "min");
        tv_arr.setText("expect arrivate time : " + arr_time + "");

        map_top.setVisibility(View.VISIBLE);
        map_bottom.setVisibility(View.VISIBLE);
        return false;
    }

    public static int px2dip(Context context, float pxValue){
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int)(pxValue / scale + 0.5f);
    }

    public static int dip2px(Context context, float dipValue){
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int)(dipValue * scale + 0.5f);
    }
}
