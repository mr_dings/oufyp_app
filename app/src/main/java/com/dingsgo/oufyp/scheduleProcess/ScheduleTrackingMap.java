package com.dingsgo.oufyp.scheduleProcess;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.Time;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dingsgo.oufyp.MainActivity;
import com.dingsgo.oufyp.MapEventPlaceInfo;
import com.dingsgo.oufyp.MapPlaceInfo;
import com.dingsgo.oufyp.Place;
import com.dingsgo.oufyp.R;
import com.dingsgo.oufyp.Schedule;
import com.dingsgo.oufyp.ScheduleUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class ScheduleTrackingMap extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, AdapterView.OnItemClickListener{

    private static final String TAG = "ScheduleTrackingMap";

    private Schedule mSchedule;
    private ScheduleUtil scheduleUtil;

    private GoogleMap mMap;
    private Marker userMarker;
    private Marker targetMarker;
    private PolylineOptions currTrackingPloyLineOptions;
    private Polyline currTrackingPloyLine;
    private Place currentTrackingPlace;
    private Map<LatLng, String> routeInstructions;
    private ArrayList<LatLng> routeOrder;
    private int currentRouteIndex = 0;

    private LocationManager mLocationManager;
    private DrawerLayout mDrawerLayout;
    private LinearLayout markerDetailZone;
    private ListView sListView;

    private TrackingPreViewAdpater mAdapter;
    private ArrayList previewList;

    private List<MapPlaceInfo> nearByinfos;
    private List<MapPlaceInfo> nearByFoods;
    private List<MapEventPlaceInfo> nearByEvents;

    private LinearLayout map_top;
    private TextView estimate_distance;
    private TextView estimate_time;
    private TextView tv_instruction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_tracking_map);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        markerDetailZone = (LinearLayout) findViewById(R.id.map_bottom);
        sListView = (ListView) findViewById(R.id.sListView);
        markerDetailZone.setVisibility(View.INVISIBLE);

        map_top = (LinearLayout) findViewById(R.id.map_top);
        estimate_distance = (TextView) findViewById(R.id.estimate_distance);
        estimate_time = (TextView) findViewById(R.id.estimate_time);
        tv_instruction = (TextView) findViewById(R.id.tv_instruction);

        Intent intent = getIntent();
        mSchedule = (Schedule) intent.getSerializableExtra("schedule");

        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

//        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if(mSchedule != null){
            scheduleUtil = new ScheduleUtil(mSchedule);
            mAdapter = new TrackingPreViewAdpater();
            sListView.setAdapter(mAdapter);
            sListView.setOnItemClickListener(this);
        }

        Log.i(TAG, "onCreate: finish");



    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Object obj = previewList.get(position);
        if(obj instanceof String){
            return;
        }
        if(obj instanceof Place){
            Place place = (Place) obj;
            MarkerOptions mo = new MarkerOptions();
            mo.position(new LatLng(place.lat, place.lon));
            mo.title(place.name);
            mo.icon(BitmapDescriptorFactory.fromResource(R.drawable.target));
            if(targetMarker != null){
                targetMarker.remove();
            }
            targetMarker = mMap.addMarker(mo);
            targetMarker.setTag(place.getMapPlaceInfo());
            currentTrackingPlace = place;
            mAdapter.notifyDataSetChanged();
            LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
            boundsBuilder.include(targetMarker.getPosition());
            boundsBuilder.include(userMarker.getPosition());
            int width = getResources().getDisplayMetrics().widthPixels;
            int height = getResources().getDisplayMetrics().heightPixels;
            mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(boundsBuilder.build(),width, height, 300));

            DownloadTask dt = new DownloadTask();
            String url = getDirectionsUrl(userMarker.getPosition(), targetMarker.getPosition());
            dt.execute(url);
        }
    }

    public void refreshMap(){
        if(mMap != null){
            mMap.clear();
            MarkerOptions mo = new MarkerOptions();
            mo.position(userMarker.getPosition());
            mo.title("you");
            mo.icon(BitmapDescriptorFactory.fromResource(R.drawable.you));
            this.userMarker = mMap.addMarker(mo);
            if(this.targetMarker != null) {
                mo = new MarkerOptions();
                mo.position(targetMarker.getPosition());
                mo.title(targetMarker.getTitle());
                mo.icon(BitmapDescriptorFactory.fromResource(R.drawable.target));
                Marker temp = mMap.addMarker(mo);
                temp.setTag(targetMarker.getTag());
                targetMarker = temp;

            }
            if(currTrackingPloyLineOptions != null)
                currTrackingPloyLine = mMap.addPolyline(currTrackingPloyLineOptions);
            if(nearByinfos != null){
                BitmapDescriptor cvs = BitmapDescriptorFactory.fromResource(R.drawable.cvs);
                BitmapDescriptor mall = BitmapDescriptorFactory.fromResource(R.drawable.plaze);
                for(MapPlaceInfo info : nearByinfos){
                    Log.i(TAG, "refreshMap: " + info.toString());
                    MarkerOptions infoMo = new MarkerOptions();
                    infoMo.position(info.getPosition());
                    infoMo.title(info.getName());
                    if(info.getType().equals("plaze"))
                        infoMo.icon(mall);
                    if(info.getType().equals("CVS"))
                        infoMo.icon(cvs);
                    mMap.addMarker(infoMo).setTag(info);
                }
            }
            if(nearByFoods != null){
                BitmapDescriptor bd = BitmapDescriptorFactory.fromResource(R.drawable.food);
                for(MapPlaceInfo foodInfo : nearByFoods){
                    Log.i(TAG, "refreshMap: " + foodInfo.toString());
                    MarkerOptions infoMo = new MarkerOptions();
                    infoMo.position(foodInfo.getPosition());
                    infoMo.title(foodInfo.getName());
                    infoMo.icon(bd);
                    mMap.addMarker(infoMo).setTag(foodInfo);
                }
            }
            if(nearByEvents != null){
                BitmapDescriptor bd = BitmapDescriptorFactory.fromResource(R.drawable.event);
                for(MapEventPlaceInfo eventInfo : nearByEvents){
                    Log.i(TAG, "refreshMap: " + eventInfo.toString());
                    MarkerOptions infoMo = new MarkerOptions();
                    infoMo.position(eventInfo.getPosition());
                    infoMo.title(eventInfo.getName());
                    infoMo.icon(bd);
                    mMap.addMarker(infoMo).setTag(eventInfo);
                }
            }
        }
    }

    private class TrackingPreViewAdpater extends BaseAdapter{


        TrackingPreViewAdpater(){
            setDate(mSchedule);
        }

        @Override
        public int getCount() {
            return previewList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        public void setDate(Schedule schedule){
            previewList = new ArrayList<>();
            int index = 0;
            for(int i = 1; i <= schedule.getDay_period(); i++){
                Schedule.DaySchedule daySchedule = schedule.getSchedule(i);
                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                long curr_day_ms = daySchedule.getDate();
                String curr_day = formatter.format(new Date(curr_day_ms));
                for(int j = 0 ; j <= 2; j++){
                    ArrayList<Place> time_schedule = daySchedule.getTimeSchedule(j);
                    if(j == 0){
                        previewList.add(curr_day + "\n am");
                    }else if(j == 1){
                        previewList.add("pm");
                    }else if(j == 2){
                        previewList.add("night");
                    }
                    for(Place place : time_schedule){
                        previewList.add(place);
                    }
                }
            }
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
//            View v = View.inflate(ScheduleTrackingMap.this, R.layout.schedule_track_list_item, null);

            View v = null;
            Object obj = previewList.get(position);
            if(obj instanceof Place){
                Place place = (Place) obj;
                v = View.inflate(ScheduleTrackingMap.this, R.layout.schedule_track_list_item, null);
                TextView tv_name = (TextView) v.findViewById(R.id.place_name);
                tv_name.setText(place.name);
                if(currentTrackingPlace != null && currentTrackingPlace.equals(place)){
                    v.setBackgroundColor(Color.RED);
                }

            }else if ( obj instanceof String){
                v = View.inflate(ScheduleTrackingMap.this, R.layout.schedule_list_category_item, null);
                TextView tv = (TextView) v.findViewById(R.id.tv_list_category);
                tv.setText(obj.toString());
            }
            return v;
        }
    }

    private void openGPSSettings() {
        LocationManager alm = (LocationManager) this
                .getSystemService(Context.LOCATION_SERVICE);
        if (alm.isProviderEnabled(android.location.LocationManager.GPS_PROVIDER)) {
            Toast.makeText(this, "GPS正常", Toast.LENGTH_SHORT).show();
            getLocation();
            return;
        } else {
            Toast.makeText(this, "请开启GPS！", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(Settings.ACTION_SECURITY_SETTINGS);
            startActivityForResult(intent, R.layout.activity_schedule_tracking_map); // 此为设置完成后返回到获取界面
            getLocation();
            return;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.map_tracking_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_information:
                if (mDrawerLayout.isDrawerOpen(GravityCompat.END))
                    mDrawerLayout.closeDrawer(GravityCompat.END);
                else
                    mDrawerLayout.openDrawer(GravityCompat.END);
                break;
            case R.id.action_request:
                Log.i(TAG, "onOptionsItemSelected: ");
                if(userMarker != null) {
                    new GetNearbyInfo().execute(userMarker.getPosition());
                    new GetOpenRice().execute(userMarker.getPosition());
                    new GetNearbyEventInfo().execute(userMarker.getPosition());
                }
                break;
        }
        return false;
    }

    private void getLocation() {
        // 获取位置管理服务

        String serviceName = Context.LOCATION_SERVICE;
        mLocationManager = (LocationManager) this.getSystemService(serviceName);
        // 查找到服务信息
        // Criteria criteria = new Criteria();
        // criteria.setAccuracy(Criteria.ACCURACY_FINE);
        // // 高精度
        // criteria.setAltitudeRequired(false);
        // criteria.setBearingRequired(false);
        // criteria.setCostAllowed(true);
        // criteria.setPowerRequirement(Criteria.POWER_LOW);
        // // 低功耗
        // String provider = locationManager.getBestProvider(criteria, true);
        // 获取GPS信息
        String provider = LocationManager.GPS_PROVIDER;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location location = mLocationManager.getLastKnownLocation(provider);// 通过GPS获取位置

        if (location == null)
            location = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        double latitude = location.getLatitude();// 经度
        double longitude = location.getLongitude();// 纬度
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 16f));
        updateToNewLocation(location);
        // 设置监听器，自动更新的最小时间为间隔N秒(1秒为1*1000)或最小位移变化超过N米
        mLocationManager.requestLocationUpdates(provider, 2000, 0,
                locationListener);
        mLocationManager.addGpsStatusListener(statusListener); // 注册状态信息回调
    }

    private List<GpsSatellite> numSatelliteList = new ArrayList<GpsSatellite>(); // 卫星信号
    /**
     * 卫星状态监听器
     */
    private final GpsStatus.Listener statusListener = new GpsStatus.Listener() {
        public void onGpsStatusChanged(int event) { // GPS状态变化时的回调，如卫星数
            if (ActivityCompat.checkSelfPermission(ScheduleTrackingMap.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            GpsStatus status = mLocationManager.getGpsStatus(null); // 取当前状态
            updateGpsStatus(event, status);
        }
    };

    private void updateGpsStatus(int event, GpsStatus status) {
        if (event == GpsStatus.GPS_EVENT_SATELLITE_STATUS) {
            int maxSatellites = status.getMaxSatellites();
            Iterator<GpsSatellite> it = status.getSatellites().iterator();
            numSatelliteList.clear();
            int count = 0;
            int usedcount = 0;
            while (it.hasNext() && count <= maxSatellites) {
                GpsSatellite s = it.next();
                numSatelliteList.add(s);
                if (s.usedInFix())
                    usedcount++;//可用卫星数量
                count++;
            }
        }
    }

    private void updateToNewLocation(Location location) {
        // 获取系统时间
        Time t = new Time();
        t.setToNow(); // 取得系统时间
        int year = t.year;
        int month = t.month + 1;
        int date = t.monthDay;
        int hour = t.hour; // 24小时制
        int minute = t.minute;
        int second = t.second;
        TextView tv1;
        tv1 = (TextView) this.findViewById(R.id.tv_body);
        if (location != null) {
            double latitude = location.getLatitude();// 经度
            double longitude = location.getLongitude();// 纬度
            double altitude = location.getAltitude(); // 海拔
            String description = "搜索卫星个数：" + numSatelliteList.size() + "/n纬度："
                    + latitude + "/n经度：" + longitude + "/n海拔：" + altitude
                    + "/n时间：" + year + "年" + month + "月" + date + "日" + hour
                    + ":" + minute + ":" + second;
//            tv1.setText(description);
            Log.i(TAG, "updateToNewLocation: " + "搜索卫星个数：" + numSatelliteList.size() + "/n纬度："
                    + latitude + "/n经度：" + longitude + "/n海拔：" + altitude
                    + "/n时间：" + year + "年" + month + "月" + date + "日" + hour
                    + ":" + minute + ":" + second);
            if (mMap != null) {
                if (userMarker != null) {
                    userMarker.remove();
                    userMarker = null;
                }
                MarkerOptions mo = new MarkerOptions();
                mo.position(new LatLng(latitude, longitude));
                mo.title("you");
                mo.icon(BitmapDescriptorFactory.fromResource(R.drawable.you));
                userMarker = mMap.addMarker(mo);
            }
        } else {

            tv1.setText("无法获取地理信息");
        }
    }

    private final LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            // 当坐标改变时触发此函数，如果Provider传进相同的坐标，它就不会被触发
            if (location != null) {
                updateToNewLocation(location);
//                Toast.makeText(ScheduleTrackingMap.this, "您的位置已发生改变！",
//                        Toast.LENGTH_SHORT).show();
            }
        }

        public void onProviderDisabled(String provider) {
            // Provider被disable时触发此函数，比如GPS被关闭
            updateToNewLocation(null);
        }

        public void onProviderEnabled(String provider) {
            // Provider被enable时触发此函数，比如GPS被打开
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
            // Provider的转态在可用、暂时不可用和无服务三个状态直接切换时触发此函数
        }
    };

    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;
        openGPSSettings();
        mMap.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
            @Override
            public void onCameraMoveStarted(int i) {
                markerDetailZone.setVisibility(View.INVISIBLE);
            }
        });
        mMap.setOnMarkerClickListener(this);
        mMap.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
            @Override
            public void onCameraMoveStarted(int reason) {
                if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
//                    Toast.makeText(ScheduleTrackingMap.this, "The user gestured on the map.",
//                            Toast.LENGTH_SHORT).show();
                    markerDetailZone.setVisibility(View.INVISIBLE);
                } else if (reason == GoogleMap.OnCameraMoveStartedListener
                        .REASON_API_ANIMATION) {
//                    Toast.makeText(ScheduleTrackingMap.this, "The user tapped something on the map.",
//                            Toast.LENGTH_SHORT).show();
                    markerDetailZone.setVisibility(View.VISIBLE);
                } else if (reason == GoogleMap.OnCameraMoveStartedListener
                        .REASON_DEVELOPER_ANIMATION) {
//                    Toast.makeText(ScheduleTrackingMap.this, "The app moved the camera.",
//                            Toast.LENGTH_SHORT).show();
                }
            }
        });
//        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
    }

    @Override
    protected void onDestroy() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            super.onDestroy();
            return;
        }
        mLocationManager.removeUpdates(locationListener);
        mLocationManager.removeGpsStatusListener(statusListener);
        mLocationManager = null;
        super.onDestroy();
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {
//        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 16f));
//        markerDetailZone.setVisibility(View.VISIBLE);
        TextView body = (TextView) markerDetailZone.findViewById(R.id.tv_body);
        TextView address = (TextView) markerDetailZone.findViewById(R.id.tv_address);
        Button btn_track = (Button) markerDetailZone.findViewById(R.id.btn_track);
        btn_track.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DownloadTask dt = new DownloadTask();
                String url = getDirectionsUrl(userMarker.getPosition(), marker.getPosition());
                dt.execute(url);
            }
        });
        if(marker.getTag() != null) {
            body.setVisibility(View.VISIBLE);
            address.setVisibility(View.VISIBLE);
            btn_track.setVisibility(View.VISIBLE);

            if(marker.getTag() instanceof MapPlaceInfo) {
                MapPlaceInfo info = (MapPlaceInfo) marker.getTag();
                body.setText(info.getName());
                address.setText(info.getAddress());
            }
            if(marker.getTag() instanceof MapEventPlaceInfo){
                MapEventPlaceInfo info = (MapEventPlaceInfo) marker.getTag();
                body.setText(info.getDescription());
                Date sd = new Date(info.getStart_time());
                Date ed = new Date(info.getEnd_time());
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd HH:mm");
                address.setText(sdf.format(sd) + " - " + sdf.format(ed));
            }
        }else{
            body.setVisibility(View.INVISIBLE);
            address.setVisibility(View.INVISIBLE);
            btn_track.setVisibility(View.INVISIBLE);

        }

        return false;
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + ","
                + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Travelling Mode
//        String mode = "mode=driving";
//        String mode = "mode=bicycling";

        float[] results=new float[1];
        Location.distanceBetween(origin.latitude, origin.longitude, dest.latitude, dest.longitude, results);

        String mode = "mode=transit";

        if(results[0] > 500){

        }else{
            mode = "mode=walking";
        }
//        String mode = "mode=walking";

        //waypoints,116.32885,40.036675
//        String waypointLatLng = "waypoints="+"40.036675"+","+"116.32885";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&"
                + mode+"&";//+waypointLatLng;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/"
                + output + "?" + parameters;
        System.out.println("getDerectionsURL--->: " + url);
        return url;
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        System.out.println("url:" + strUrl + "---->   downloadurl:" + data);
        return data;
    }

    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();   

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    private class ParserTask extends
            AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(
                String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);

                // Starts parsing data
                routes = parse(jObject);
                System.out.println("do in background:" + routes);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(3);

                // Changing the color polyline according to the mode
                lineOptions.color(Color.BLUE);
            }

            // Drawing polyline in the Google Map for the i-th route
            if(currTrackingPloyLine != null){
                currTrackingPloyLine.remove();
                currTrackingPloyLine = null;
            }
            currTrackingPloyLineOptions = lineOptions;
            currTrackingPloyLine = mMap.addPolyline(lineOptions);

            currentRouteIndex = 0;
            if(routeOrder.size() > 0){
                tv_instruction.setText(routeInstructions.get(routeOrder.get(currentRouteIndex)));
            }else{
                tv_instruction.setText("");
            }

        }
    }

    public List<List<HashMap<String, String>>> parse(JSONObject jObject) {

        List<List<HashMap<String, String>>> routes = new ArrayList<List<HashMap<String, String>>>();
        JSONArray jRoutes = null;
        JSONArray jLegs = null;
        JSONArray jSteps = null;

        routeInstructions = new HashMap<>();
        routeOrder = new ArrayList<>();
        int index = 0;
        try {

            jRoutes = jObject.getJSONArray("routes");

            /** Traversing all routes */
            for (int i = 0; i < jRoutes.length(); i++) {
                jLegs = ((JSONObject) jRoutes.get(i)).getJSONArray("legs");
                List path = new ArrayList<HashMap<String, String>>();

                /** Traversing all legs */
                for (int j = 0; j < jLegs.length(); j++) {
                    jSteps = ((JSONObject) jLegs.get(j)).getJSONArray("steps");

                    final JSONObject Jdistance = ((JSONObject) jLegs.get(j)).getJSONObject("distance");
                    final JSONObject Jtime = ((JSONObject) jLegs.get(j)).getJSONObject("duration");

                    map_top.post(new Runnable() {
                        @Override
                        public void run() {
                            map_top.setVisibility(View.VISIBLE);
                            try {
                                estimate_distance.setText("Estimate distance : "+Jdistance.getString("text"));
                                estimate_time.setText("Estimate time : " + Jtime.getString("text"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            ;
                        }
                    });

                    /** Traversing all steps */
                    for (int k = 0; k < jSteps.length(); k++) {
                        String polyline = "";
                        polyline = (String) ((JSONObject) ((JSONObject) jSteps
                                .get(k)).get("polyline")).get("points");
                        List<LatLng> list = decodePoly(polyline);

                        /** Traversing all points */
                        for (int l = 0; l < list.size(); l++) {
                            HashMap<String, String> hm = new HashMap<String, String>();
                            hm.put("lat",
                                    Double.toString(((LatLng) list.get(l)).latitude));
                            hm.put("lng",
                                    Double.toString(((LatLng) list.get(l)).longitude));
                            path.add(hm);
                        }

                        JSONObject endPoint = (JSONObject) ((JSONObject)jSteps.get(k)).get("end_location");

                        LatLng ll = new LatLng(endPoint.getDouble("lat"), endPoint.getDouble("lng"));
                        String instruction = ((JSONObject)jSteps.get(k)).getString("html_instructions");

                        routeInstructions.put(ll, instruction);
                        routeOrder.add(index++, ll);

                    }
                    routes.add(path);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
        }
        return routes;
    }

    /**
     * Method to decode polyline points Courtesy :
     * jeffreysambells.com/2010/05/27
     * /decoding-polylines-from-google-maps-direction-api-with-java
     * */
    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }
        return poly;
    }

    public class GetNearbyInfo extends AsyncTask<LatLng, Void, List<MapPlaceInfo>> {

            public static final String HOST = "http://fyp.dingsgo.com/api/search";
//        public static final String HOST = "http://192.168.1.219/fyp/index.php?/api/search";
        private static final String TAG = "GetNearbyInfo";

        @Override
        protected List<MapPlaceInfo> doInBackground(LatLng... params) {
            String getParam = "?lat=" + params[0].latitude + "&lng=" + params[0].longitude;
            String response = getURlResponse(HOST + getParam);
            Log.i(TAG, "doInBackground: " + response);
            ArrayList<MapPlaceInfo> infos = null;
            try {
                JSONObject json = new JSONObject(response);
                JSONArray nl = json.getJSONArray("results");
                infos = new ArrayList<>();
                for(int i = 0; i < nl.length(); i++){
                    JSONObject nlJson = nl.getJSONObject(i);
                    MapPlaceInfo info = MapPlaceInfo.parseJSON(nlJson);
                    infos.add(info);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return infos;
        }

        @Override
        protected void onPostExecute(List<MapPlaceInfo> infos) {
            nearByinfos = infos;
            refreshMap();
        }
    }

    private String getURlResponse(String s) {
        HttpURLConnection httpURLConnection = null;
        InputStream inputStream = null;
        String resultData = "";

        try {
            URL url = new URL(s);   //URl对象
            Log.i(TAG, "getURlResponse: url = " + url);
            httpURLConnection = (HttpURLConnection) url.openConnection();   //使用URl打开一个链接
            httpURLConnection.setDoInput(true);//允许输入流，即允许下载
            httpURLConnection.setDoOutput(true);//允许输出流，即允许上传
            httpURLConnection.setUseCaches(false); //不使用缓冲
            httpURLConnection.setRequestMethod("GET"); //使用get请求

            int responseCode = httpURLConnection.getResponseCode();
            Log.i(TAG, "getURlResponse: response code = " + responseCode);

            inputStream = httpURLConnection.getInputStream();//获取输入流，此时才真正建立链接
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String inputLine;
            while ((inputLine = bufferedReader.readLine()) != null) {
                resultData = inputLine + resultData + "\n";
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputStream != null)
                    inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            httpURLConnection.disconnect();
        }
        return resultData;
    }

    public class GetOpenRice extends AsyncTask<LatLng, Void, ArrayList<MapPlaceInfo>> {

        public static final String request = "";
        public static final String HOST = "http://fyp.dingsgo.com/api/searchFood";

        @Override
        protected ArrayList<MapPlaceInfo> doInBackground(LatLng... params) {
            String getParam = "?lat=" + params[0].latitude + "&lng=" + params[0].longitude;
            String response = getURlResponse(HOST + getParam);
            ArrayList<MapPlaceInfo> foods = null;

            try {
                JSONObject json = new JSONObject(response);
                JSONObject pR = json.getJSONObject("paginationResult");
                JSONArray results = pR.getJSONArray("results");
                foods = new ArrayList<>();
                for(int i = 0; i < results.length(); i++){
                    JSONObject result = results.getJSONObject(i);
                    String name = result.getString("name");
                    String address = result.getString("address");
                    LatLng ll = new LatLng(result.getDouble("mapLatitude"), result.getDouble("mapLongitude"));
                    String type = "food";
                    MapPlaceInfo info = new MapPlaceInfo(name, type, ll, address);
                    foods.add(info);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return foods;

        }

        @Override
        protected void onPostExecute(ArrayList<MapPlaceInfo> mapPlaceInfos) {
            nearByFoods = mapPlaceInfos;
            refreshMap();
        }
    }
    public class GetNearbyEventInfo extends AsyncTask<LatLng, Void, List<MapEventPlaceInfo>> {

        public static final String HOST = "http://fyp.dingsgo.com/api/searchEvent";
        //        public static final String HOST = "http://192.168.1.219/fyp/index.php?/api/search";
        private static final String TAG = "GetNearbyInfo";

        @Override
        protected List<MapEventPlaceInfo> doInBackground(LatLng... params) {
            String getParam = "?lat=" + params[0].latitude + "&lng=" + params[0].longitude;
            String response = getURlResponse(HOST + getParam);
            Log.i(TAG, "doInBackground: " + response);
            ArrayList<MapEventPlaceInfo> infos = null;
            try {
                JSONObject json = new JSONObject(response);
                JSONArray nl = json.getJSONArray("results");
                infos = new ArrayList<>();
                for(int i = 0; i < nl.length(); i++){
                    JSONObject nlJson = nl.getJSONObject(i);
                    MapEventPlaceInfo info = MapEventPlaceInfo.parseJSON(nlJson);
                    infos.add(info);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return infos;
        }

        @Override
        protected void onPostExecute(List<MapEventPlaceInfo> infos) {
            nearByEvents = infos;
            refreshMap();
        }
    }
}
