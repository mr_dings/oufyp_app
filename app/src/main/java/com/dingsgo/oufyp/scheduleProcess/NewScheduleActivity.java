package com.dingsgo.oufyp.scheduleProcess;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatDrawableManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dingsgo.oufyp.R;
import com.dingsgo.oufyp.ResultActivity;
import com.dingsgo.oufyp.Schedule;
import com.dingsgo.oufyp.Tourist;
import com.dingsgo.oufyp.TouristManager;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class NewScheduleActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = "NewScheduleActivity";

    private Toolbar toolbar;

    private EditText et_schedule_name;
    private Button btn_pick_date, btn_trip_day, btn_next;
    private TextView tv_date, tv_day;
    Calendar c = Calendar.getInstance();

    private int mDay, mMonth, mYear, day_period;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_schedule);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        ActionBar bar = getSupportActionBar();
        setSupportActionBar(toolbar);
        setTitle("Add Schedule");
//        toolbar.setNavigationIcon(android.R.drawable.ic_menu_revert);
        toolbar.setNavigationIcon(AppCompatDrawableManager.get().getDrawable(this, R.drawable.ic_arrow_back_white_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                NewScheduleActivity.this.setResult(RESULT_CANCELED);
                NewScheduleActivity.this.finish();
            }
        });

        et_schedule_name = (EditText) findViewById(R.id.et_schedule_name);
        btn_pick_date = (Button) findViewById(R.id.btn_pick_date);
        btn_trip_day = (Button) findViewById(R.id.btn_trip_day);
        btn_next = (Button) findViewById(R.id.btn_next);
        tv_date = (TextView) findViewById(R.id.tv_date);
        tv_day = (TextView) findViewById(R.id.tv_day);

        btn_trip_day.setOnClickListener(this);
        btn_pick_date.setOnClickListener(this);
        btn_next.setOnClickListener(this);

        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        tv_date.setText("Choose date is : " + mDay + "/" + mMonth + "/" + mYear);

        day_period = 2;
        tv_day.setText(day_period + " day");

    }

    private void showSimpleListDialog() {
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
//        builder.setIcon(R.mipmap.ic_launcher);
//        builder.setTitle("choose");

        /**
         * 设置内容区域为简单列表项
         */
        final String[] Items={"1 day","2 day","3 day","4 day","5 day","6 day","7 day"};
        builder.setItems(Items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(getApplicationContext(), "You clicked "+Items[i], Toast.LENGTH_SHORT).show();
                day_period = ++i;
                tv_day.setText(i + " day");
            }
        });
        builder.setCancelable(true);
        AlertDialog dialog=builder.create();
        dialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_pick_date:
                new DatePickerDialog(this, dateListener, mYear, mMonth, mDay).show();
                break;
            case R.id.btn_trip_day:
                showSimpleListDialog();
                break;
            case R.id.btn_next:
                String name = et_schedule_name.getText().toString().trim();
                if(name.isEmpty()){
                    Toast.makeText(this, "name can not be empty.", Toast.LENGTH_LONG).show();
                    return;
                }
//                Calendar ca = Calendar.getInstance();
//                ca.set(mYear,mMonth - 1,mDay);
                String dateStr = mYear + "";
                if(mMonth >= 10){
                    dateStr += mMonth;
                }else{
                    dateStr += 0 + "" + mMonth;
                }
                if(mDay >= 10){
                    dateStr += mDay;
                }else{
                    dateStr += 0 + "" + mDay;
                }
                DateFormat df = new SimpleDateFormat("yyyyMMdd");
                Date d = null;
                try {
                    d = df.parse(dateStr);
                    Log.i(TAG, "datStr : " + dateStr);
                    Log.i(TAG, "onClick: " + df.format(d));
                    Log.i(TAG, "date time : " + d.getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
//                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                System.out.println(formatter.format(ca.getTime()));
//                System.out.println(ca.getTimeInMillis());
//                System.out.println(System.currentTimeMillis());
                Schedule s = new Schedule(name, d.getTime(), day_period);
                TouristManager tm = new TouristManager(getApplicationContext());
                ArrayList<Tourist> list = tm.getAll();
//                for(Tourist t: list){
//                    s.add(1, Schedule.TIME_MORNING, t);
//                }
                try {
                    Schedule.saveAsLocalStorage(getApplicationContext(), s);
                    Intent intent = new Intent();
                    intent.setClass(getApplicationContext(), ScheduleEditActivity.class);
                    intent.putExtra("schedule", s);
                    startActivity(intent);
                    this.finish();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener(){

        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            mDay = dayOfMonth;
            mMonth = month + 1;
            mYear = year;
            tv_date.setText("Choose date is : " + mDay + "/" + mMonth + "/" + mYear);
        }
    };
}
