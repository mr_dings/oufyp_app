package com.dingsgo.oufyp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dings on 25/1/2017.
 */

public class TouristManager implements DBManager<Tourist> {

    public static final String HOST = "http://fyp.dingsgo.com/";
    private Context context;
    private FypDB db;

    public TouristManager(Context context) {
        this.context = context;
        db = new FypDB(this.context);
    }

    public long insert(Tourist tourist) {
        SQLiteDatabase db = this.db.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put("name", tourist.getName());
        cv.put("district", tourist.getDistrict());
        cv.put("address", tourist.getAddress());
        cv.put("description", tourist.getDescription());
        cv.put("detail", tourist.getDetail());
        cv.put("website", tourist.getWebsite());
        cv.put("lat", tourist.getLat());
        cv.put("lon", tourist.getLon());
        cv.put("tel", tourist.getTel());
        cv.put("imgSrc", tourist.getImgSrc());
        cv.put("tag1", tourist.getTag1());
        cv.put("tag2", tourist.getTag2());
        cv.put("tag3", tourist.getTag3());
        cv.put("openHour", tourist.openHour);
        cv.put("endHour", tourist.endHour);
        cv.put("recommendMinute", tourist.recommendMinute);

        long row = db.insert(FypDB.TOURISTS_TABLE, null, cv);
        db.close();
        return row;
    }

    public ArrayList<Tourist> getAll() {
        SQLiteDatabase db = this.db.getReadableDatabase();
        Cursor cursor = db
                .query(FypDB.TOURISTS_TABLE, null, null, null, null, null, null);
        ArrayList<Tourist> tourists = new ArrayList<Tourist>();
        while (cursor.moveToNext()) {
            Tourist tourist = getRowTourist(cursor);
            tourists.add(tourist);
        }
        db.close();
        return tourists;
    }

    public Tourist getRamdomTourist(){
        SQLiteDatabase db = this.db.getReadableDatabase();
        Cursor cursor = db
                .query(FypDB.TOURISTS_TABLE, null, null, null, null, null, " random()", "1");
        if(cursor.moveToNext()){
            Tourist tourist = getRowTourist(cursor);
            return tourist;
        }
        return null;
    }

    private void removeAll(){
        SQLiteDatabase db = this.db.getWritableDatabase();
        db.delete(FypDB.TOURISTS_TABLE, null, null);
    }

    public Tourist get(String name) {
        SQLiteDatabase db = this.db.getReadableDatabase();
        Cursor cursor = db
                .query(FypDB.TOURISTS_TABLE, null, "name = ?", new String[]{name}, null, null, null);
        Tourist tourist = null;
        if (cursor.moveToNext()) {
            tourist = getRowTourist(cursor);
        }
        return tourist;
    }

    public ArrayList<Tourist> query(String keyword) {
        String sql = "select * from " + FypDB.TOURISTS_TABLE + " where name like ?";
        SQLiteDatabase db = this.db.getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, new String[]{"%" + keyword + "%"});
        ArrayList<Tourist> tourists = new ArrayList<Tourist>();
        while (cursor.moveToNext()) {
            Tourist tourist = getRowTourist(cursor);
            tourists.add(tourist);
        }
        db.close();
        return tourists;
    }

    public ArrayList<Tourist> getCategory(String category) {
        String sql = "select * from " + FypDB.TOURISTS_TABLE + " where tag1 = ? or tag2 = ? or tag3 = ?";
        SQLiteDatabase db = this.db.getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, new String[]{category, category, category});
        ArrayList<Tourist> tourists = new ArrayList<Tourist>();
        while (cursor.moveToNext()) {
            Tourist tourist = getRowTourist(cursor);
            tourists.add(tourist);
        }
        db.close();
        return tourists;
    }

    public ArrayList<Tourist> getTouirstWithLocation(){
        String sql = "select * from " + FypDB.TOURISTS_TABLE + " where lat != 0 and lon != 0 and recommendMinute <= 300";
        SQLiteDatabase db = this.db.getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        ArrayList<Tourist> tourists = new ArrayList<Tourist>();
        while (cursor.moveToNext()) {
            Tourist tourist = getRowTourist(cursor);
            tourists.add(tourist);
        }
        db.close();
        return tourists;
    }

    public ArrayList<Tourist> exist(Place place){
        String sql = "select * from " + FypDB.TOURISTS_TABLE + " where name not in ( ? )";
        SQLiteDatabase db = this.db.getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, new String[]{place.name});
        ArrayList<Tourist> tourists = new ArrayList<Tourist>();
        while (cursor.moveToNext()) {
            Tourist t = getRowTourist(cursor);
            tourists.add(t);
        }
        db.close();
        return tourists;
    }

    @NonNull
    private Tourist getRowTourist(Cursor cursor) {
        Tourist tourist = new Tourist();
        tourist.name = cursor.getString(cursor.getColumnIndex("name"));
        tourist.district = cursor.getString(cursor.getColumnIndex("district"));
        tourist.address = cursor.getString(cursor.getColumnIndex("address"));
        tourist.description = cursor.getString(cursor.getColumnIndex("description"));
        tourist.detail = cursor.getString(cursor.getColumnIndex("detail"));
        tourist.lat = cursor.getDouble(cursor.getColumnIndex("lat"));
        tourist.lon = cursor.getDouble(cursor.getColumnIndex("lon"));
        tourist.tel = cursor.getString(cursor.getColumnIndex("tel"));
        tourist.website = cursor.getString(cursor.getColumnIndex("website"));
        tourist.imgSrc = cursor.getString(cursor.getColumnIndex("imgSrc"));
        tourist.tag1 = cursor.getString(cursor.getColumnIndex("tag1"));
        tourist.tag2 = cursor.getString(cursor.getColumnIndex("tag2"));
        tourist.tag3 = cursor.getString(cursor.getColumnIndex("tag3"));
        tourist.openHour = cursor.getInt(cursor.getColumnIndex("openHour"));
        tourist.endHour = cursor.getInt(cursor.getColumnIndex("endHour"));
        tourist.recommendMinute = cursor.getInt(cursor.getColumnIndex("recommendMinute"));
        return tourist;
    }

    public class GetAllTouristTask extends AsyncTask<String, Integer, List<Tourist>> {

        public static final String TAG = "GetAllTouristTask";

        private Context context;

        public GetAllTouristTask(Context context) {
            this.context = context;
        }

        //onPreExecute方法用于在执行后台任务前做一些UI操作
        @Override
        protected void onPreExecute() {
            Log.i(TAG, "onPreExecute() called");

        }

        //doInBackground方法内部执行后台任务,不可在此方法内修改UI
        @Override
        protected List<Tourist> doInBackground(String... params) {
            //用POST发送JSON数据
            String data = getURlResponse(params[0]);
            try {
                JSONObject json = new JSONObject(data);
                JSONArray tourists = json.getJSONArray("tourists");
                ArrayList<Tourist> list = new ArrayList<Tourist>();
                for (int i = 0; i < tourists.length(); i++) {
                    JSONObject JSONTourist = tourists.getJSONObject(i);
                    Tourist tourist = Tourist.parseJSON(JSONTourist);
                    list.add(tourist);
                    Log.i(TAG, "doInBackground: tourst -> " + tourist.toString());
                }
                return list;
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Tourist> list) {
            if (list != null) {
                // success action
                TouristManager tm = new TouristManager(context);
                tm.removeAll();
                for (Tourist tourist : list) {
                    tm.insert(tourist);
                }
            }
            // fail action
        }

        private String getURlResponse(String s) {
            HttpURLConnection httpURLConnection = null;
            InputStream inputStream = null;
            String resultData = "";

            try {
                URL url = new URL(s);   //URl对象
                Log.i(TAG, "getURlResponse: url = " + url);
                httpURLConnection = (HttpURLConnection) url.openConnection();   //使用URl打开一个链接
                httpURLConnection.setDoInput(true);//允许输入流，即允许下载
                httpURLConnection.setDoOutput(true);//允许输出流，即允许上传
                httpURLConnection.setUseCaches(false); //不使用缓冲
                httpURLConnection.setRequestMethod("GET"); //使用get请求

                int responseCode = httpURLConnection.getResponseCode();
                Log.i(TAG, "getURlResponse: response code = " + responseCode);

                inputStream = httpURLConnection.getInputStream();//获取输入流，此时才真正建立链接
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String inputLine;
                while ((inputLine = bufferedReader.readLine()) != null) {
                    resultData = inputLine + resultData + "\n";
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (inputStream != null)
                        inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                httpURLConnection.disconnect();
            }
            return resultData;
        }
    }


}
