package com.dingsgo.oufyp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private ArrayList<Place> placeList;
    private static final String TAG = "MapActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        Intent intent = getIntent();
        placeList = (ArrayList<Place>) intent.getSerializableExtra("places");

        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we
     * just add a marker near Africa.
     */
    @Override
    public void onMapReady(GoogleMap map) {

        LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();

        for(Place place : placeList){
            MarkerOptions mo = new MarkerOptions();
            if(place.lat == 0 && place.lon == 0){
                continue;
            }
            LatLng ln = new LatLng(place.lat, place.lon);
            boundsBuilder.include(ln);
            mo.position(ln);
            mo.title(place.name);
            mo.snippet("test");
            map.addMarker(mo);
        }
//        int cameraWidth = dip2px(this, 160);
//        int cameraheight = dip2px(this, 160);

        if(placeList.size() > 1) {
            int width = getResources().getDisplayMetrics().widthPixels;
            int height = getResources().getDisplayMetrics().heightPixels;
            map.moveCamera(CameraUpdateFactory.newLatLngBounds(boundsBuilder.build(), width, height, 100));
//            map.moveCamera(CameraUpdateFactory.newLatLngBounds(boundsBuilder.build(), 100));
        }else{
            LatLng ll = new LatLng(placeList.get(0).lat, placeList.get(0).lon);
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(ll,17f));
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    public static int px2dip(Context context, float pxValue){
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int)(pxValue / scale + 0.5f);
    }

    public static int dip2px(Context context, float dipValue){
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int)(dipValue * scale + 0.5f);
    }
}