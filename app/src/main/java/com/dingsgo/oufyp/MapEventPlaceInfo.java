package com.dingsgo.oufyp;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by dings on 25/4/2017.
 */

public class MapEventPlaceInfo {

    private String name;
    private String type;
    private LatLng position;
    private String description;
    private long start_time;
    private long end_time;

    private MapEventPlaceInfo(){

    }

    public MapEventPlaceInfo(String name, String type, LatLng position, String description, long start_time, long end_time) {
        this.name = name;
        this.type = type;
        this.position = position;
        this.description = description;
        this.start_time = start_time;
        this.end_time = end_time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public LatLng getPosition() {
        return position;
    }

    public void setPosition(LatLng position) {
        this.position = position;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getStart_time() {
        return start_time;
    }

    public void setStart_time(long start_time) {
        this.start_time = start_time;
    }

    public long getEnd_time() {
        return end_time;
    }

    public void setEnd_time(long end_time) {
        this.end_time = end_time;
    }

    public static MapEventPlaceInfo parseJSON(JSONObject obj) throws JSONException {
        MapEventPlaceInfo info = new MapEventPlaceInfo();
        info.setName(obj.getString("name"));
        info.setType(obj.getString("type"));
        info.setDescription(obj.getString("description"));
        info.setStart_time(obj.getLong("starttime"));
        info.setEnd_time(obj.getLong("durtime"));

        JSONObject loc = obj.getJSONObject("loc");
        JSONArray coord = loc.getJSONArray("coordinates");
        LatLng ll = new LatLng(coord.getDouble(1), coord.getDouble(0));
        info.setPosition(ll);
        return info;
    }

    @Override
    public String toString() {
        return "MapEventPlaceInfo{" +
                "name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", position=" + position +
                ", description='" + description + '\'' +
                ", start_time=" + start_time +
                ", end_time=" + end_time +
                '}';
    }
}
