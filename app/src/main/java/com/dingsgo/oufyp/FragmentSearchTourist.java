package com.dingsgo.oufyp;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentSearchTourist#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentSearchTourist extends Fragment implements SearchView.OnQueryTextListener , View.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    String autoString[] = new String[]{"a","abc","ccc"};

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
//    private ListView lv;


    public FragmentSearchTourist() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentSearchTourist.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentSearchTourist newInstance(String param1, String param2) {
        FragmentSearchTourist fragment = new FragmentSearchTourist();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_tourist, container, false);
//        Button btn_all = (Button) v.findViewById(R.id.btn_all);
        Button btn_high_light = (Button) v.findViewById(R.id.btn_high_light);
        Button btn_culture = (Button) v.findViewById(R.id.btn_culture);
        Button btn_art_entertainment = (Button) v.findViewById(R.id.btn_art_entertainment);
        Button btn_out_door = (Button) v.findViewById(R.id.btn_out_door);
        btn_high_light.setOnClickListener(this);
        btn_culture.setOnClickListener(this);
        btn_art_entertainment.setOnClickListener(this);
        btn_out_door.setOnClickListener(this);
//        btn_all.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onQueryTextSubmit("");
//            }
//        });
//        lv = (ListView) v.findViewById(R.id.lv);
//        lv.setAdapter(new ArrayAdapter<String>(getContext(),android.R.layout.simple_list_item_1, autoString));
//        lv.setTextFilterEnabled(true);//设置lv可以被过虑


        final SearchView sv = (SearchView) v.findViewById(R.id.sv);
        // 设置该SearchView默认是否自动缩小为图标
        sv.setIconifiedByDefault(false);
        // 为该SearchView组件设置事件监听器
        sv.setOnQueryTextListener(this);
        // 设置该SearchView显示搜索按钮
        sv.setSubmitButtonEnabled(false);
        // 设置该SearchView内默认显示的提示文本
        sv.setQueryHint("search keyword");

//        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                sv.setQuery(autoString[position],true);
//                onQueryTextSubmit(autoString[position]);
//            }
//        });

        return v;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        // 实际应用中应该在该方法内执行实际查询
        // 此处仅使用Toast显示用户输入的查询内容
        Toast.makeText(getContext(), "您的选择是:" + query, Toast.LENGTH_SHORT).show();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
        Intent intent = new Intent();
        intent.setClass(getContext(),ResultActivity.class);

        Bundle bundle = new Bundle();
        bundle.putString("keyword", query);
        bundle.putInt("category", ResultActivity.CATEGORY_TOURIST);
        intent.putExtra("data", bundle);

        startActivityForResult(intent, ResultActivity.KEYWORD_SEARCH);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String query) {

        Toast.makeText(getContext(), "textChange--->" + query, Toast.LENGTH_LONG).show();

        if (TextUtils.isEmpty(query)) {
            // 清除ListView的过滤
//            lv.clearTextFilter();
        } else {
            // 使用用户输入的内容对ListView的列表项进行过滤
//            lv.setFilterText(query);
        }
        return true;

    }

    @Override
    public void onClick(View v) {
        String category = "";
        switch (v.getId()){
            case R.id.btn_high_light:
                category = Tourist.CATEGORY_HIGHT_LIGHT;
                break;
            case R.id.btn_culture:
                category = Tourist.CATEGORY_CULTURE;
                break;
            case R.id.btn_art_entertainment:
                category = Tourist.CATEGORY_ART_ENTERTAINMENT;
                break;
            case R.id.btn_out_door:
                category = Tourist.CATEGORY_OUTDOOR;
                break;
        }
        Intent intent = new Intent();
        intent.setClass(getContext(), ResultActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt("category", ResultActivity.CATEGORY_TOURIST);
        bundle.putString("query", category);
        intent.putExtra("data", bundle);
        startActivity(intent);
    }
}
