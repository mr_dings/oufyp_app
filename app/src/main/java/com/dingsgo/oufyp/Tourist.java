package com.dingsgo.oufyp;

import android.location.Location;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by dings on 25/1/2017.
 */

public class Tourist extends Place implements Serializable{

    public static final String CATEGORY_HIGHT_LIGHT = "必遊景點";
    public static final String CATEGORY_CULTURE = "古今文化";
    public static final String CATEGORY_ART_ENTERTAINMENT = "文娛藝術";
    public static final String CATEGORY_OUTDOOR = "戶外探索";

    private static final long serialVersionUID = 385893275928L;

    public String tag1;
    public String tag2;
    public String tag3;

    public Tourist(String name, String district, String address, String description, String detail, String website, double lat, double lon, String tel, String imgSrc, String tag1, String tag2, String tag3) {
        this.name = name;
        this.district = district;
        this.address = address;
        this.description = description;
        this.detail = detail;
        this.website = website;
        this.lat = lat;
        this.lon = lon;
        this.tel = tel;
        this.imgSrc = imgSrc;
        this.tag1 = tag1;
        this.tag2 = tag2;
        this.tag3 = tag3;
    }

    public Tourist(){

    }

    public String getTag1() {
        return tag1;
    }

    public void setTag1(String tag1) {
        this.tag1 = tag1;
    }

    public String getTag2() {
        return tag2;
    }

    public void setTag2(String tag2) {
        this.tag2 = tag2;
    }

    public String getTag3() {
        return tag3;
    }

    public void setTag3(String tag3) {
        this.tag3 = tag3;
    }

    public static Tourist parseJSON(JSONObject object){
        try {
            Tourist tourist = new Tourist();
            tourist.name = object.getString("name");
            tourist.district = object.getString("district");
            tourist.address = object.getString("address");
            tourist.description = object.getString("description");
            tourist.detail = object.getString("detail");
            tourist.website = object.getString("website");
            tourist.tel = object.getString("tel");
            tourist.imgSrc = object.getString("imgSrc");
            tourist.tag1 = object.getString("tag1");
            tourist.tag2 = object.getString("tag2");
            tourist.tag3 = object.getString("tag3");
            tourist.lat = object.getDouble("lat");
            tourist.lon = object.getDouble("lon");
            tourist.openHour = object.getInt("openHour");
            tourist.endHour = object.getInt("endHour");
            tourist.recommendMinute = object.getInt("recommendMinute");

            return tourist;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String toString() {
        return "Tourist{" +
                "tag1='" + tag1 + '\'' +
                ", tag2='" + tag2 + '\'' +
                ", tag3='" + tag3 + '\'' +
                "place=" + super.toString() +
                '}';
    }
}
