package com.dingsgo.oufyp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.widget.ImageView;

import com.google.android.gms.maps.model.BitmapDescriptor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by dings on 26/1/2017.
 */

public class ImageManager {

    private static final String TAG = "ImageManager";

    private Bitmap bitmap;

    private NetCacheUtils netCacheUtils;
    private LocalCacheUtils localCacheUtils;
    private MemoryCacheUtils memoryCacheUtils;
    private Context context;

    public ImageManager(Context context) {
        this.context = context;
        netCacheUtils = new NetCacheUtils();
        localCacheUtils = new LocalCacheUtils();
        memoryCacheUtils = new MemoryCacheUtils();
    }

    /**
     * 加载图片，将当前URL对应的图片显示到ivPic的控件上
     *
     * @param ivPic
     *            ImageView控件
     * @param url
     *            图片的地址
     */
    public void display(ImageView ivPic, String url) {
        // 设置默认显示的图片
        ivPic.setImageResource(R.drawable.ic_menu_gallery);
//        System.out.println(HOST + url);

        // 1、内存缓存
        bitmap = memoryCacheUtils.getBitmapFromMemory(MainActivity.HOST + url);
        if (bitmap != null) {
            ivPic.setImageBitmap(bitmap);
//            System.out.println("从内存缓存中加载图片");
            return;
        }
        // 2、本地磁盘缓存
        bitmap = localCacheUtils.getBitmapFromLocal(MainActivity.HOST + url);
        if (bitmap != null) {
            ivPic.setImageBitmap(bitmap);
//            System.out.println("从本地SD卡加载的图片");
            memoryCacheUtils.setBitmap2Memory(url, bitmap);// 将图片保存到内存
            return;
        }
        // 3、网络缓存
//        System.out.println("从network加载的图片");
        netCacheUtils.getBitmapFromNet(ivPic, MainActivity.HOST, url);
		/*
		 * 从网络获取图片之后，将图片保存到手机SD卡中，在进行图片展示的时候，优先从SD卡中读取缓存,key是图片的URL的MD5值，
		 * value是保存的图片bitmap
		 */
    }
    public Bitmap get(String url){
        bitmap = memoryCacheUtils.getBitmapFromMemory(MainActivity.HOST + url);
        if (bitmap != null) {
            return bitmap;
        }
        // 2、本地磁盘缓存
        bitmap = localCacheUtils.getBitmapFromLocal(MainActivity.HOST + url);
        if (bitmap != null) {

//            System.out.println("从本地SD卡加载的图片");
            memoryCacheUtils.setBitmap2Memory(url, bitmap);// 将图片保存到内存
            return bitmap;
        }
        return null;
    }

    private class LocalCacheUtils {
        /**
         * 文件保存的路径
         */
//        public final String FILE_PATH = Environment
//                .getExternalStorageDirectory().getAbsolutePath() + "/cache/image";
        public final String FILE_PATH = context.getFilesDir().getAbsolutePath();

        /**
         * 从本地SD卡获取网络图片，key是url的MD5值
         *
         * @param url
         * @return
         */
        public Bitmap getBitmapFromLocal(String url) {
            try {
                String fileName = MD5EncodeUtil.encode(url);
                File file = new File(FILE_PATH, fileName);
                if (file.exists()) {
                    Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(
                            file));
                    return bitmap;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;

        }

        /**
         * 向本地SD卡写网络图片
         *
         * @param url
         * @param bitmap
         */
        public void setBitmap2Local(String url, Bitmap bitmap) {
            try {
                // 文件的名字
                String fileName = MD5EncodeUtil.encode(url);
                // 创建文件流，指向该路径，文件名叫做fileName
                File file = new File(FILE_PATH, fileName);
                // file其实是图片，它的父级File是文件夹，判断一下文件夹是否存在，如果不存在，创建文件夹
                File fileParent = file.getParentFile();
                if (!fileParent.exists()) {
                    // 文件夹不存在
                    fileParent.mkdirs();// 创建文件夹
                }
                // 将图片保存到本地
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100,
                        new FileOutputStream(file));

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private class MemoryCacheUtils {
        /*
         * 由于map默认是强引用，所有在JVM进行垃圾回收的时候不会回收map的引用
         */
        // private HashMap map = new HashMap();
        // 软引用的实例,在内存不够时，垃圾回收器会优先考虑回收
        // private HashMap> mSoftReferenceMap = new
        // HashMap>();
        // LruCache
        private LruCache<String, Bitmap> lruCache;

        public MemoryCacheUtils() {
            // lruCache最大允许内存一般为Android系统分给每个应用程序内存大小（默认Android系统给每个应用程序分配16兆内存）的八分之一（推荐）
            // 获得当前应用程序运行的内存大小
            long mCurrentMemory = Runtime.getRuntime().maxMemory();
            int maxSize = (int) (mCurrentMemory / 8);
            // 给LruCache设置最大的内存
            lruCache = new LruCache<String, Bitmap>(maxSize) {
                @Override
                protected int sizeOf(String key, Bitmap value) {
                    // 获取每张图片所占内存的大小
                    // 计算方法是：图片显示的宽度的像素点乘以高度的像素点
                    int byteCount = value.getRowBytes() * value.getHeight();// 获取图片占用内存大小
                    return byteCount;
                }
            };
        }

        /**
         * 从内存中读取Bitmap
         *
         * @param url
         * @return
         */
        public Bitmap getBitmapFromMemory(String url) {

            // Bitmap bitmap = map.get(url);
            // SoftReference softReference = mSoftReferenceMap.get(url);
            // Bitmap bitmap = softReference.get();
            // 软引用在Android2.3以后就不推荐使用了，Google推荐使用lruCache
            // LRU--least recently use
            // 最近最少使用,将内存控制在一定的大小内，超过这个内存大小，就会优先释放最近最少使用的那些东东
            Bitmap bitmap = (Bitmap) lruCache.get(url);
            return bitmap;

        }

        /**
         * 将图片保存到内存中
         *
         * @param url
         * @param bitmap
         */
        public void setBitmap2Memory(String url, Bitmap bitmap) {
            // 向内存中设置，key,value的形式，首先想到HashMap
            // map.put(url, bitmap);
            // 保存软引用到map中
            // SoftReference mSoftReference = new
            // SoftReference(bitmap);
            // mSoftReferenceMap.put(url, mSoftReference);
            lruCache.put(url, bitmap);
        }

    }

    private class NetCacheUtils {
        String url;
        public void getBitmapFromNet(ImageView ivPic, String host, String url) {
            DownImgAsyncTask task = new DownImgAsyncTask(ivPic);
            this.url = url;
            task.execute(host + url);
        }

        private class DownImgAsyncTask extends AsyncTask<String, Void, Bitmap> {
            private ImageView ivPic;
            private static final String TAG = "DownImgAsyncTask";

            public DownImgAsyncTask(ImageView ivPic) {
                this.ivPic = ivPic;
            }

//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            imageView.setImageBitmap(null);
//            showProgressBar();//显示进度条提示框
//        }

            @Override
            protected Bitmap doInBackground(String[] params) {

                bitmap = memoryCacheUtils.getBitmapFromMemory(MainActivity.HOST + url);
                if (bitmap != null) {
                    return bitmap;
                }
                // 2、本地磁盘缓存
                bitmap = localCacheUtils.getBitmapFromLocal(MainActivity.HOST + url);
                if (bitmap != null) {
                    memoryCacheUtils.setBitmap2Memory(url, bitmap);// 将图片保存到内存
                    return bitmap;
                }

                Bitmap b = getImageBitmap(String.valueOf(params[0]));
                if(b == null)
                    return null;
                memoryCacheUtils.setBitmap2Memory(params[0],b);
                localCacheUtils.setBitmap2Local(params[0],b);
                return b;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                if(bitmap != null)
                    ivPic.setImageBitmap(bitmap);
                else
                    ivPic.setImageResource(R.drawable.error404);
            }
            private Bitmap getImageBitmap(String url) {
//                Log.i(TAG, "getImageBitmap: url = " + url);
                URL imgUrl = null;
                Bitmap bitmap = null;
                HttpURLConnection httpURLConnection = null;
                try {
                    imgUrl = new URL(url);
//                    Log.i(TAG, "getImageBitmap: mark");
                    httpURLConnection = (HttpURLConnection) imgUrl.openConnection();
//                    Log.i(TAG, "getImageBitmap: mark2");
//                    httpURLConnection.connect();
//                    Log.i(TAG, "getImageBitmap: response code = " + httpURLConnection.getResponseCode());;
                    InputStream inputStream = httpURLConnection.getInputStream();
                    bitmap = BitmapFactory.decodeStream(inputStream);
                    inputStream.close();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }finally{
                    httpURLConnection.disconnect();
                }
                return bitmap;
            }
        }


    }
}
