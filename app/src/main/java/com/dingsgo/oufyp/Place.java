package com.dingsgo.oufyp;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;

/**
 * Created by dings on 31/1/2017.
 */

public class Place implements Serializable{
    private static final long serialVersionUID = 9835093185342L;

    public String name;
    public String district;
    public String address;
    public String description;
    public String detail;
    public String website;
    public double lat;
    public double lon;
    public String tel;
    public String imgSrc;
    public int openHour;
    public int endHour;
    public int recommendMinute;
    public long startDate;
    public long endDate;
    public String openingHoursDescription;
    public String admission;


    public int getOpenHour() {
        return openHour;
    }

    public void setOpenHour(int openHour) {
        this.openHour = openHour;
    }

    public int getEndHour() {
        return endHour;
    }

    public void setEndHour(int endHour) {
        this.endHour = endHour;
    }

    public int getRecommendMinute() {
        return recommendMinute;
    }

    public void setRecommendMinute(int recommendMinute) {
        this.recommendMinute = recommendMinute;
    }

    public long getStartDate() {
        return startDate;
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }

    public long getEndDate() {
        return endDate;
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }

    public String getOpeningHoursDescription() {
        return openingHoursDescription;
    }

    public void setOpeningHoursDescription(String openingHoursDescription) {
        this.openingHoursDescription = openingHoursDescription;
    }

    public String getAdmission() {
        return admission;
    }

    public void setAdmission(String admission) {
        this.admission = admission;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getImgSrc() {
        return imgSrc;
    }

    public void setImgSrc(String imgSrc) {
        this.imgSrc = imgSrc;
    }

    public MapPlaceInfo getMapPlaceInfo(){
        MapPlaceInfo mpi = new MapPlaceInfo(this.name, "place", new LatLng(lat, lon), address);
        return mpi;
    }

    public static double getDistance(double lat1, double lon1, double lat2, double lon2) {
        float[] results=new float[1];
        Location.distanceBetween(lat1, lon1, lat2, lon2, results);
        return results[0];
    }

    public static double getDistance(Place tourist1, Place tourist2){
        return getDistance(tourist1.lat, tourist1.lon, tourist2.lat, tourist2.lon);
    }

    @Override
    public String toString() {
        return "Place{" +
                "name='" + name + '\'' +
                ", district='" + district + '\'' +
                ", address='" + address + '\'' +
                ", description='" + description + '\'' +
                ", detail='" + detail + '\'' +
                ", website='" + website + '\'' +
                ", lat=" + lat +
                ", lon=" + lon +
                ", tel='" + tel + '\'' +
                ", imgSrc='" + imgSrc + '\'' +
                ", openHour=" + openHour +
                ", endHour=" + endHour +
                ", recommendMinute=" + recommendMinute +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", openingHoursDescription='" + openingHoursDescription + '\'' +
                ", admission='" + admission + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Place place = (Place) o;

        return this.name.equals(place.name);

    }
}
